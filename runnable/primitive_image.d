#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/+dub.sdl:
name "primitive_image"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module primitive_image;

import
    core.thread, std.stdio, std.path;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts;

OsWindow win1;

void main(string[] args)
{
    win1 = construct!OsWindow;
    win1.beginRealign;
    win1.left   = 250;
    win1.top    = 250;
    win1.width  = 600;
    win1.height = 400;

    Image img = win1.addControl!Image(Rect(0,0,10,10), Alignment.client);
    img.bitmap.loadFromPng(__FILE__.dirName ~ "/../media/acorn.png");
    img.stretch = Stretch.scaled;
    win1.endRealign;

    publisherToFile(img, "primg.txt");

    kheopsRun;
}
