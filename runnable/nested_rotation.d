#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype
module nested_rotation;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts,
    kheops.helpers.polygons;

OsWindow win1;

struct Handler
{
}

void main()
{
    win1 = construct!OsWindow;

    win1.beginRealign;
    win1.left   = 1;
    win1.top    = 1;
    win1.width  = 900;
    win1.height = 600;

    Handler h;

    Rectangle r0 = win1.addControl!Rectangle(Rect(20,20,300,300), Alignment.none);
    r0.fill.color = ColorConstant.bisque;
    r0.clipChildren = true;

    Rectangle r1 = r0.addControl!Rectangle(Rect(20,20,300,300), Alignment.client);
    r1.fill.color = ColorConstant.bisque;
    r1.rotationAngle = 22.5;
    r1.clipChildren = true;

    Rectangle r2 = r1.addControl!Rectangle(Rect(20,20,300,300), Alignment.client);
    r2.fill.color = ColorConstant.bisque;
    r2.rotationAngle = 22.5;

    win1.endRealign;
    kheopsRun;
}
