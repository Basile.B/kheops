#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "split_layout"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module split_layout;

import
    core.thread, std.stdio, std.path;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.primitives, kheops.layouts;

OsWindow win;
SplitLayout spl;

void main(string[] args)
{
    win = construct!OsWindow;
    win.beginRealign;
    win.left   = 250;
    win.top    = 250;
    win.width  = 600;
    win.height = 400;

    win.caption = "Splitted layout - MW | LMB + drag | click area | +/-";

    spl = win.addControl!SplitLayout(Rect(0,0,1,1), Alignment.client);
    Rectangle bf = spl.beforeControl.addControl!Rectangle(Rect(0,0,1,1), Alignment.client);
    Rectangle af = spl.afterControl.addControl!Rectangle(Rect(0,0,1,1), Alignment.client);

    Handler h;

    spl.onKeyDown = &h.kd;

    af.fill.color = ColorConstant.bisque;
    af.onMouseDown = &h.md;
    af.wantMouse = true;

    bf.fill.color = ColorConstant.bisque;
    bf.onMouseDown = &h.md;
    bf.wantMouse = true;

    win.endRealign;
    kheopsRun;
}

struct Handler
{
    void md(Object, double, double, ref const MouseButtons mb, ref const KeyModifiers)
    {
        if (MouseButton.left !in mb)
            return;
        if (spl.splitAxis == Axis.horizontal)
            spl.splitAxis = Axis.vertical;
        else
            spl.splitAxis = Axis.horizontal;
    }

    void kd(Object, dchar key, ref const KeyModifiers km)
    {
        const double ss = spl.splitterSize;
        if (/*KeyModifier.ctrl in km &&*/ key == '+' && ss <= 30)
            spl.splitterSize = ss + 2;
        else if (/*KeyModifier.ctrl in km &&*/ key == '-' && ss >= 2)
            spl.splitterSize = ss - 2;
    }
}

