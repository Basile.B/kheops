#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype -g -gs

/+dub.sdl:
name "mouse_and_events"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module mouse_and_events;

import
    std.stdio, std.string;
import
    iz.memory;
import
    kheops.types, kheops.control, kheops.colors, kheops.brush,
    kheops.primitives, kheops.canvas, kheops.timers;

OsWindow win;
Handler handler;
IdleTimer it;

void main(string[] args)
{
    win = construct!OsWindow;
    win.caption = "animated primitives: CTRL+CLICK | WHEEL | CTRL+WHEEL | DRAG";
    it = construct!IdleTimer;
    it.interval = 10;
    it.onTimer = &handler.idelTimerTimer;
    it.start;
    win.onMouseDown = &handler.windowMouseDown;
    kheopsRun;
}

static ~this()
{
    destruct(it);
}

struct Handler
{
    void ctrlKeyDown(Object notifier, dchar key, const ref KeyModifiers km)
    {
        Control ct = cast(Control) notifier;
        switch (key)
        {
            case VirtualKey.VK_LEFT: ct.left = ct.left - 5; break;
            case VirtualKey.VK_RIGHT: ct.left = ct.left + 5; break;
            case VirtualKey.VK_UP: ct.top = ct.top - 5; break;
            case VirtualKey.VK_DOWN: ct.top = ct.top + 5; break;
            default:
        }
    }

    void windowMouseDown(Object notifier, double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        if (mb != [MouseButton.left] || km != [KeyModifier.ctrl])
            return;

        Ctrl ct = win.addControl!Ctrl(Rect(x-20,y-20,40,40));

        ct.onPaint      = &ctrlPaint;
        ct.onMouseWheel = &ctrlMw;
        ct.onMouseDown  = &ctrlMouseDown;
        ct.onMouseUp    = &ctrlMouseUp;
        ct.onMouseMove  = &ctrlMouseMove;
        ct.onKeyDown    = &ctrlKeyDown;
        ct.caption      = format("ctrl index %d", ct.siblingIndex);

        win.repaint;
    }

    void ctrlMw(Object notifier, byte delta, const ref KeyModifiers km)
    {
        Control ct = cast(Control) notifier;
        auto pt = win.mouse.position;
        int inc = delta * -5;
        if (km == [KeyModifier.ctrl])
        {
            ct.left = ct.left - inc;
            win.mouse.position = PointI32(pt.x - inc, pt.y);
        }
        else
        {
            ct.top = ct.top + inc;
            win.mouse.position = PointI32(pt.x, pt.y + inc);
        }
        win.repaint;
    }

    void ctrlPaint(Object notifier, const ref Rect rect, ref Canvas canvas)
    {
        auto wincc = win.sizeList;
        double wt = ubyte.max / wincc[0];
        double ht = ubyte.max / wincc[1];

        Control ctrl = cast(Control) notifier;
        auto ctrlcc = ctrl.parentedRectList;
        auto ctrlcx = ctrlcc[0] + ctrlcc[2] * 0.5f;
        auto ctrlcy = ctrlcc[1] + ctrlcc[3] * 0.5f;

        canvas.rectangle(0, 0, ctrlcc[2..$]);
        canvas.fillBrush.beginChange;
        canvas.fillBrush.rgba.a = ubyte.max;
        canvas.fillBrush.rgba.r = cast(ubyte) cast(int)((wincc[0] - ctrlcx) * wt);
        canvas.fillBrush.rgba.g = cast(ubyte) cast(int)(ctrlcx * wt);
        canvas.fillBrush.rgba.b = cast(ubyte) cast(int)((wincc[1] - ctrlcy) * ht);
        canvas.fillBrush.endChange;
        canvas.pen.width = 2;
        canvas.fill!false;
        canvas.stroke!true;
        canvas.text(0, 12, ctrl.caption);
    }

    void ctrlMouseDown(Object notifier, double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        Ctrl ct = cast(Ctrl) notifier;
        ct.down = true;
        ct.downX = x;
        ct.downY = y;
    }

    void ctrlMouseUp(Object notifier, double x, double y, const ref MouseButtons mb,
        const ref KeyModifiers km)
    {
        Ctrl ct = cast(Ctrl) notifier;
        ct.down = false;
    }

    void ctrlMouseMove(Object notifier, double x, double y, const ref KeyModifiers km)
    {
        Ctrl ct = cast(Ctrl) notifier;
        if (!ct.down) return;
        ct.beginRealign;
        ct.left =  ct.left + x - ct.downX;
        ct.top =  ct.top + y - ct.downY;
        ct.endRealign;
        ct.repaint;
    }

    void idelTimerTimer(Object notifier)
    {
        foreach(child; win.children)
        {
            if (child.focused) continue;
            (cast(Ctrl) child).animate;
        }
        win.repaint;
    }
}

class Ctrl: Control
{
    mixin inheritedDtor;

    double downX, downY;
    bool grow;
    bool down;

    void animate()
    {
        if (grow)
        {
            auto cc = parentedRectList;
            sizePosition(cc[0] - 0.5f, cc[1] - 0.5f, cc[2] + 1f, cc[3] + 1f);
            if (width > 60) grow = false;
            rotationAngle = rotationAngle + 5;
        }
        else
        {
            auto cc = parentedRectList;
            sizePosition(cc[0] + 0.5f, cc[1] + 0.5f, cc[2] - 1f, cc[3] - 1f);
            if (width < 10) grow = true;
            rotationAngle = rotationAngle - 5;
        }
    }
}

