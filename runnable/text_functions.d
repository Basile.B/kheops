#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype
module text_functions;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts;


OsWindow win1;

class TestCtrl: Control
{
    mixin inheritedDtor;
    uint rndCol;
    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);

        canvas.pen.width = 2.0f;
        canvas.fillBrush.fillKind = FillKind.uniform;
        canvas.fillBrush.color = ColorConstant.white;
        canvas.rectangle(0, 0, width, height);
        canvas.fill!false;
        canvas.stroke!true;

        canvas.font.name = "DejaVuSansMono";
        canvas.font.style = [FontStyle.bold, FontStyle.italic];
        canvas.font.size = 12;
        canvas.text(40,40, "azertyuiop^QSDFGHJKLM--é€à");

        canvas.font.name = "CasualLigaturePro";
        canvas.font.style = [FontStyle.bold, FontStyle.italic];
        canvas.font.size = 60;
        canvas.drawText(40,90, "azertyuiop^QSDFGHJKLM-é€à");
        canvas.fillBrush.color = ColorConstant.red;
        canvas.fill!false;
        canvas.stroke!true;
    }
}


void main(string[] args)
{
    win1 = construct!OsWindow;
    win1.beginRealign;
    win1.left   = 250;
    win1.top    = 250;
    win1.width  = 900;
    win1.height = 120;

    win1.addControl!TestCtrl(Rect(0,0,10,10), Alignment.client);
    win1.endRealign;

    kheopsRun;
}
