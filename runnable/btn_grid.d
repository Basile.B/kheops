#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/+dub.sdl:
name "btn_grid"
dependency "kheops" path="../"
libs "X11" platform="posix"
libs "cairo" "freetype"
+/

module btn_grid;

import
    core.thread, std.stdio;
import
    iz.memory, iz.properties, iz.streams, iz.serializer;
import
    kheops.types, kheops.control, kheops.canvas,
    kheops.colors, kheops.pen, kheops.primitives, kheops.layouts;


OsWindow win;

@TellRangeAdded
class Btn: Control
{

    mixin inheritedDtor;

private:

    Text _text;
    Rectangle _background;
    RectangleEn _foreground;
    bool _down;
    bool _over;

    enum _stateStr = [["up","down"],[">> up <<",">> down <<"]];

    double[4][2] _gradPos = [
        [0, 0.2, 0.8 ,1],
        [0, 0.4, 0.6, 1],
    ];
    uint[4][3][2] _gradCols = [[
        [0xAA505050, 0xAA808080, 0xAA808080 ,0xAA505050],
        [0xAA505050, 0xAA208050, 0xAA208050 ,0xAA505050],
        [0xAA505050, 0xAA505050, 0xAA505050 ,0xAA505050],
    ],[
        [0xAA505050, 0xAA208050, 0xAA208050 ,0xAA505050],
        [0xAA505010, 0xAA208050, 0xAA208050 ,0xAA505010],
        [0xAA505010, 0xAA208050, 0xAA208050 ,0xAA505010],
    ]];

    void updateState()
    {

        _foreground.fill.beginChange();
        _foreground.fill.gradient.colors = _gradCols[focused][_down + _over];
        _foreground.fill.gradient.positions = _gradPos[_down];
        _foreground.fill.endChange();
        caption = _stateStr[focused][_down];
        repaint;
    }

    void mouseDown(Object not, double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (mb == [MouseButton.left])
        {
            _down = true;
            updateState;
        }
    }

    void mouseUp(Object not, double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        _down = false;
        updateState;
    }

    void mouseLeave(Object not)
    {
        _over = false;
        _down = _leftMbDown;
        updateState;
    }

    void mouseEnter(Object not)
    {
        _over = true;
        updateState;
    }

protected:

    override void unsetFocus()
    {
        updateState;
    }

    override void setFocus()
    {
        updateState;
    }

public:

    this()
    {
        _background = addControl!Rectangle(Rect(0,0,1,1), Alignment.client);
        _foreground = addControl!RectangleEn(Rect(0,0,1,1), Alignment.client);
        _text = addControl!Text(Rect(0,0,1,1), Alignment.client);

        _foreground.marginAll = 3;

        _background.fill.color = ColorConstant.white;
        _foreground.fill.fillKind = FillKind.gradient;
        _foreground.fill.gradientKind = GradientKind.vertical;
        _foreground.radius = 5;

        _text.wantMouse = true;
        _text.onMouseDown = &mouseDown;
        _text.onMouseUp = &mouseUp;
        _text.onMouseLeave = &mouseLeave;
        _text.onMouseEnter = &mouseEnter;
        _text.font.size = 12;
        _text.font.style = [FontStyle.bold];
        _text.font.name = "DejaVuSansMono";
        _text.justify = Justify.center;
        _text.fill.color = ColorConstant.black;
        _text.stroke.color = ColorConstant.black;
        _text.pen.width = 1;

        clipChildren = true;

        wantKeys = true;
        updateState;
    }

    @Set override void caption(string value)
    {
        super.caption(value);
        if (_text)
            _text.caption = value;
    }
}

void main()
{
    win = construct!OsWindow;

    win.caption = "100 * (3 primitives + 1 container) = 400 controls";

    win.beginRealign;
    win.left   = 20;
    win.top    = 20;
    win.width  = 900;
    win.height = 600;

    AxisLayout al0 = win.addControl!(AxisLayout)(Rect(0,0,1,1), Alignment.client);
    foreach(i; 0 .. 10)
    {
        AxisLayout col = al0.addControl!(AxisLayout)(Rect(0,0,1,1));
        col.axis = Axis.vertical;
        foreach(j; 0 .. 10)
        {
            Control c = col.addControl!Btn(Rect(0,0,1,1));
            assert(c.parent);
            c.tabOrder = 10 - j;
        }
    }

    win.endRealign;
    kheopsRun;
}
