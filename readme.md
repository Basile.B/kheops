# Kheops

Kheops was an attempt (2015-2017) to create a vectorial GUI framework, based on Cairo and written in D.
It was also a test ground for the [iz](https://gitlab.com/basile.b/iz) library to the extent that _Kheops_ helped 
much to detect bugs or errors that did not happen in  D `unittest`.

# Setup

Only linux is suported. 

## Dependencies

As static libraries,

- x11, 
- cairo and 
- freetype

They are usually installed with the "devel" version of the respective package.

## Build

- dub:
  - `dub build`
  - as examples, most of the runnables includes a SDL receipt so that they can be launched with `dub <fname.d>`
- dexed-ide:
  - opens the dub.json as a project, compiles the project and register it in the library manager.
  - the examples can only be run with `Run DUB single file package`, 
    unless the other dependencies ([imagesformats](https://github.com/lgvz/imageformats), 
    [x11](https://github.com/nomad-software/x11) and [iz](https://gitlab.com/basile.b/iz)) are registered in the libman,
    in which case, `Execute runnable module`, works too.
