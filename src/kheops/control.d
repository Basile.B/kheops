#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype
/**
 * Bases classes such has window and control.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.control;

import
    std.math, std.typecons, std.stdio, std.algorithm.comparison, std.uni,
    std.traits, std.conv;
import
    cairo.cairo;
import
    iz.memory, iz.containers, iz.properties, iz.enumset, iz.serializer,
    iz.streams, iz.math, iz.classes;
import
    kheops.types, kheops.canvas, kheops.styles, kheops.timers, kheops.actions,
    kheops.helpers.keys, kheops.font, kheops.bitmap;

static this()
{
    registerFactoryClasses!(OsWindow, CustomMenuWindow, MenuItem, Brush)(classesRepository);
}

/**
 * Provides the informations for a string, as drawn in a Canvas.
 *
 * This static class is usefull for the controls that have to estimate
 * the dimension of a text before being drawn or to prevent too much
 * operation on each repaint.
 */
class textInfo
{

private:

    static __gshared cairo_t* _cr;
    static __gshared cairo_surface_t* _sf;

    static this()
    {
        _sf = cairo_image_surface_create(CairoFormat.A8, 16, 16);
        _cr = cairo_create(_sf);
    }

    static ~this()
    {
        cairo_surface_destroy(_sf);
        cairo_destroy(_cr);
    }

public:

    /**
     * Sets the pen
     */
    static void setPen(Pen pen)
    {
        cairo_set_line_width(_cr, pen.width);
    }

    /**
     * Sets the font
     */
    static void setFont(kheops.font.Font font)
    {
        import kheops.fontloader;
        import cairo.ft;
        cairo_font_face_t* ff = getCairoFont(font.name);
        cairo_set_font_face(_cr, ff);
        cairo_set_font_size(_cr, font.size);
        cairo_ft_font_face_set_synthesize(ff, font.style.container & 3);

        import std.string: fromStringz;
        assert(cairo_font_face_status(ff) == CairoStatus.Success,
            cairo_font_face_status(ff).cairo_status_to_string.fromStringz);
    }

    /**
     *  Gets the information.
     */
    static TextExtent opCall(const(char)[] text)
    {
        import std.string: toStringz;
        cairo_text_extents_t ex = void;
        cairo_text_extents(_cr, text.toStringz , &ex);
        return TextExtent(ex.x_advance, ex.height, ex.x_bearing, ex.y_bearing);
    }
}


//TODO-cbugfix: When default enum values are not serialized, restoration is incorrect
// see addIstNodeForDescriptor(), handling of PropHint.initcare

/**
 * The big class that implements most of the fonctionaities of a control,
 * including: positioning, realignement, visibility, user input, styling,
 * painting, action, serialization, treeitem traits.
 */
class CustomControl: PropertyPublisher
{

    mixin TreeItem;
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    // seems that the problem is that this alias is lazy evaluated.
    //alias DynPub = PublishedObjectArray!(CustomControl, false, "");

    bool _visible;
    bool _wantKeys, _wantMouse;
    bool _clipChildren;
    bool _focused;
    bool _scaled;

    Event _onMouseEnter;
    Event _onMouseLeave;
    Event _onVisibilityChanged;
    Event _onClick, _onDoubleClick;
    Event _onFocusedChanged;
    Event _onRealigned;

    MouseDownEvent  _onMouseDown;
    MouseUpEvent    _onMouseUp;
    MouseMoveEvent  _onMouseMove;
    MouseWheelEvent _onMouseWheel;

    KeyDownEvent _onKeyDown;
    KeyUpEvent _onKeyUp;

    PaintEvent _onPaint;

    Alignment _alignment;

    Rect _parentedRect;
    Rect _sizeRect;
    Rect _drawRect;
    Margin _margin;
    Margin _padding;
    double _rotationAngle = 0.0f;
    Point _scalingRatio = Point(1,1);

    DrawableBitmap _bmp;

    Array!char _styler;
    //PublishedObjectArray!(CustomControl, false, "") _dynamicPublications;

    Action _action;

    // Default alignment methods ----------------------------------------------+
    alias Aligner = void delegate(CustomControl item, ref Rect space);
    alias Aligners = EnumProcs!(Alignment, Aligner);
    Aligners aligners;

    void toNone(CustomControl item, ref Rect space)
    {}
    void toLeft(CustomControl item, ref Rect space)
    {
        item.beginRealign;
            if (item._scaled)
            {
                item.width = item.width * item._scalingRatio.x;
                item._scalingRatio.x = 1;
                item._scalingRatio.y = 1;
            }
            item.left   = space.left + item._margin.left;
            space.left  += item.width + item._margin.right + item._margin.left;
            space.width -= item.width + item._margin.right + item._margin.left;
            item.height = space.height - item._margin.top - item._margin.bottom;
            item.top    = space.top + item._margin.top;
        item.endRealign;
    }
    void toRight(CustomControl item, ref Rect space)
    {
        item.beginRealign;
            if (item._scaled)
            {
                item.width = item.width * item._scalingRatio.x;
                item._scalingRatio.x = 1;
                item._scalingRatio.y = 1;
            }
            item.left   = space.width + space.left - item.width - item._margin.right;
            space.width -= item.width + item._margin.right + item._margin.left;
            item.height = space.height - item._margin.top - item._margin.bottom;
            item.top    = space.top + item._margin.top;
        item.endRealign;
    }
    void toTop(CustomControl item, ref Rect space)
    {
        item.beginRealign;
            if (item._scaled)
            {
                item.height = item.height * item._scalingRatio.y;
                item._scalingRatio.x = 1;
                item._scalingRatio.y = 1;
            }
            item.left   = space.left + item._margin.left;
            item.width  = space.width - item._margin.right - item._margin.left;
            item.top    = space.top + item._margin.top;
            space.top   += item.height + item._margin.top + item._margin.bottom;
            space.height-= item.height + item._margin.top + item._margin.bottom;
        item.endRealign;
    }
    void toBottom(CustomControl item, ref Rect space)
    {
        item.beginRealign;
            if (item._scaled)
            {
                item.height = item.height * item._scalingRatio.y;
                item._scalingRatio.x = 1;
                item._scalingRatio.y = 1;
            }
            item.left   = space.left + item._margin.left;
            item.width  = space.width - item._margin.right - item._margin.left;
            item.top    = space.top + space.height - item.height - item._margin.top;
            space.height-= item.height + item._margin.top + item._margin.bottom;
        item.endRealign;
    }
    void toClient(CustomControl item, ref Rect space)
    {
        item.beginRealign;
            item.left   = space.left + item._margin.left;
            item.top    = space.top + item._margin.top;
            item.width  = space.width - item._margin.left - item._margin.right;
            item.height = space.height - item._margin.top - item._margin.bottom;
        item.endRealign;
    }

    void updateScalingRatio(double newW, double newH)
    {
        if (newH == 0 || newW == 0)
            return;

        const double rx = 1 / (_sizeRect.width / newW);
        const double ry = 1 / (_sizeRect.height / newH);

        foreach (CustomControl c; children)
        {
            if (c._scaled)
            {
                if (c._scalingRatio.x == 1) c._scalingRatio.x = rx;
                if (c._scalingRatio.y == 1) c._scalingRatio.y = ry;
            }
            else
            {
                c._scalingRatio.x = 1;
                c._scalingRatio.y = 1;
            }
        }
    }
    // ----

    // Misc. ------------------------------------------------------------------+
    void handleActionChanged(Object)
    {
        updateFromAction;
    }

    //TODO-cpadding: updateDrawRect() relies on _sizeRect, it must be updated at the same time
    void updateDrawRect()
    {
        _drawRect = _sizeRect;
        _drawRect.width  = _sizeRect.width - (_padding.left + _padding.right);
        _drawRect.height = _sizeRect.height - (_padding.top + _padding.bottom);
        _drawRect.left   = _padding.left;
        _drawRect.top    = _padding.top;
    }
    // -------------------------------------------------------------------------

protected:

    Point _downPos;
    Array!char _caption, _hint;
    bool _leftMbDown;
    bool _focusable;
    int _tabOrder;
    int _index;
    bool _restoreIndex;
    bool _updateTabs;
    cairo_matrix_t _matrix;
    cairo_matrix_t _imatrix;
    CustomMenuWindow _contextMenu;
    bool _design;
    bool _designable = true;
    bool _designResizeX;
    bool _designResizeY;
    bool _needRepaint = true;

    version (show_need_repaint)
    static DrawableBitmap _updateMark;

    // Alignment --------------------------------------------------------------+

    /**
     * begin/endRealign counter.
     *
     * It should be always reset to 0 in realign() overrides.
     */
    ptrdiff_t _realignCount, _realignLock;

    /**
     * Allow to implements a custom way to realign the children.
     *
     * This virtual methods is called, by default, after realignChildren in
     * realign(). kheops.layout classes override realign() to skip realignChildren()
     * and override this method to perform their custom alignment.
     */
    void customRealignChildren(){}
    // ----

    // Visibility -------------------------------------------------------------+

    /**
     * If it's assigned, call the event onVisibilityChanged.
     */
    final void doVisibilityChanged()
    {
        if (_onVisibilityChanged)
            _onVisibilityChanged(this);
    }

    /**
     * Set the visible state of the control and call onVisibilityChanged.
     *
     * This method is final but according to the value procShow() or procHide()
     * are called.
     */
    final void setVisible(bool value)
    {
        if (_visible == value)
            return;
        _visible = value;
        _visible ? procShow : procHide;
        doVisibilityChanged;
    }

    /**
     * Virtual method called by setVisible() when the control pass from hidden to
     * visible.
     *
     * The default implementation tries to realign.
     */
    void procShow() {tryRealign;}

    /**
     * Virtual method called by setVisible() when the control pass from visible to
     * hidden. The base implemenation is empty.
     */
    void procHide() {}

    /**
     * Sets the focus state. Not to be called manually.
     */
    final void internalSetFocus(bool value)
    {
        if (_focused == value)
            return;
        _focused = value;
        value ? setFocus : unsetFocus;
        if (_onFocusedChanged)
            _onFocusedChanged(this);
    }

    /**
     * Virtual method called when the control has earned the focus.
     * The state of the focus is managed by internalSetFocus(), a call to the
     * method actually doesn't changes the value returned by focused().
     */
    void setFocus() {}

    /**
     * Virtual method called when the control looses the focus.
     * See setFocus().
     */
    void unsetFocus() {}
    // ----

    // Mouse input ------------------------------------------------------------+
    bool _penetrated;

    /**
     * If it's assigned, call the event onMouseEnter.
     */
    final void doMouseEnter()
    {
        if (_onMouseEnter)
            _onMouseEnter(this);
    }

    /**
     * If it's assigned, call the event onMouseLeave.
     */
    final void doMouseLeave()
    {
        if (_onMouseLeave)
            _onMouseLeave(this);
    }

    /**
     * Virtual method called when the mouse enters the control.
     * If an override doesn't call super() then doMouseEnter() must also be called.
     */
    void procMouseEnter() {_penetrated = true; doMouseEnter;}

    /**
     * Virtual method called when the mouse leaves the control.
     * If an override doesn't call super() then doMouseLeve() must also be called.
     */
    void procMouseLeave() {_penetrated = false; doMouseLeave;}

    /**
     * Virtual method called when the mouse is pressed on the control.
     * In an override, onMouseDown() must either be called via super() or manually.
     *
     * Params:
     *      x = The x mouse position, based on the parent coordinate system and transformations.
     *      y = The y mouse position, based on the parent coordinate system and transformations.
     *      mb = A set representing the mouse buttons that are pressed.
     *      km = A set representing the key modifiers that are pressed.
     */
    void procMouseDown(double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (_contextMenu && mb == [MouseButton.right])
        {
            Point pos = toWindowCoordinates(Point(x,y));
            _contextMenu.show(pos.x, pos.y);
        }
        _downPos.x = x;
        _downPos.y = y;
        if (_design && _designable)
            procDesignMouseDown(x, y, mb, km);
        if (_onMouseDown)
            _onMouseDown(this, x, y, mb, km);
    }

    /// Same as procMouseDown but called at design time.
    void procDesignMouseDown(double x, double y,
        const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (_alignment == Alignment.none)
        {
            _designResizeX = x > sizeRect.width - 5;
            _designResizeY = y > sizeRect.height - 5;
        }
        else
        {
            _designResizeX = false;
            _designResizeY = false;
        }
    }

    /**
     * Virtual method called when the mouse is released over the control.
     * In an override, the context menu must be handled and onMouseUp() must
     * either be called via super.procMouseUp() or manually.
     *
     * Params:
     *      x = The x mouse position, based on the parent coordinate system and transformations.
     *      y = The y mouse position, based on the parent coordinate system and transformations.
     *      mb = A set representing the mouse buttons that are pressed.
     *      km = A set representing the key modifiers that are pressed.
     */
    void procMouseUp(double x, double y, const ref MouseButtons mb, const ref KeyModifiers km)
    {
        if (_design && _designable)
            procDesignMouseUp(x, y, mb, km);
        if (_onMouseUp)
            _onMouseUp(this, x, y, mb, km);
    }

    /// Same as procMouseUp but called at design time.
    void procDesignMouseUp(double x, double y,
        const ref MouseButtons mb, const ref KeyModifiers km)
    {

    }

    /**
     * Virtual method called when the mouse moves over the control.
     * In an override, onMouseMove() must either be called via super() or manually.
     *
     * Params:
     *      x = The x mouse position, based on the parent coordinate system and transformations.
     *      y = The y mouse position, based on the parent coordinate system and transformations.
     *      km = A set representing the key modifiers that are pressed.
     */
    void procMouseMove(double x, double y, const ref KeyModifiers km)
    {
        if (_design && _designable)
            procDesignMouseMove(x, y, km);
        if (_onMouseMove)
            _onMouseMove(this, x, y, km);
    }

    /// Same as procMouseMove but called at design time.
    void procDesignMouseMove(double x, double y,
        const ref KeyModifiers km)
    {
        if (parent && _leftMbDown && _focused)
        {
            beginRealign;
            if (_designResizeX)
            {
                width = max(1, x);
            }
            else if (_designResizeY)
            {
                height = max(1, y);
            }
            else
            {
                const double newX = _parentedRect.left + x - _downPos.x;
                const double newY = _parentedRect.top + y - _downPos.y;
                left = newX;
                top = newY;
            }
            endRealign;
            repaint;
        }
    }

    /**
     * Virtual method called when the mouse wheel is used.
     * In an override, onMouseWheel() must either be called via super() or manually.
     */
    void procMouseWheel(byte delta, const ref KeyModifiers km)
    {
        if (_onMouseWheel)
            _onMouseWheel(this, delta, km);
    }

    /**
     * Virtual method called after a left mouse button click
     * In an override, onClick() must either be called via super() or manually.
     */
    void procClick()
    {
        if (_onClick)
            _onClick(this);
    }

    /**
     * Virtual method called after a double left mouse button click
     * In an override, onDoubleClick() must either be called via super() or manually.
     */
    void procDoubleClick()
    {
        if (_onDoubleClick)
            _onDoubleClick(this);
    }
    // ----

    // Keyboard input ---------------------------------------------------------+
    /**
     * Virtual method called when a keyboard key is pressed
     * In an override, onKeyDown() must either be called via super() or manually
     * and procTabNext() should be called when key is equal to character 9.
     *
     * Params:
     *      key = Either a character or a virtual key.
     *      km  = A set that represents the status of the CTRL, SHIFT and ALT keys.
     */
    void procKeyDown(dchar key, const ref KeyModifiers km)
    {
        if (key == '\t')
            procTabNext;
        if (_onKeyDown)
            _onKeyDown(this, key, km);
    }

    /**
     * Virtual method called when a keyboard key is released
     * In an override, onKeyUp() must either be called via super() or manually.
     *
     * Params:
     *      key = Either a character or a virtual key.
     *      km  = A set that represents the status of the CTRL, SHIFT and ALT keys.
     */
    void procKeyUp(dchar key, const ref KeyModifiers km)
    {
        if (_onKeyUp)
            _onKeyUp(this, key, km);
    }

    void procTabNext()
    {
        internalSetFocus(false);
        CustomControl nextFocused;
        CustomControl lowest;
        ptrdiff_t max = siblingCount + 1;
        ptrdiff_t min = _tabOrder;
        // nearest greater order
        foreach(CustomControl sibling; siblings)
        {
            if (!sibling.focusable)
                continue;
            if (sibling._tabOrder < max && sibling._tabOrder > _tabOrder)
            {
                max = sibling._tabOrder;
                nextFocused = sibling;
            }
            else if (sibling._tabOrder < min)
            {
                lowest = sibling;
                min = sibling.tabOrder;
            }
        }
        // orderered, back to smallest
        if (!nextFocused && lowest)
            nextFocused = lowest;
        // next focusable with order set to 0
        if (!nextFocused && nextSibling)
        {
            nextFocused = nextSibling;
            while (nextFocused && !nextFocused.focusable && nextFocused._tabOrder < 0)
                nextFocused = nextFocused.nextSibling;
        }
        // back to first
        if (!nextFocused && firstSibling.focusable)
            nextFocused = firstSibling;

        if (nextFocused)
        {
            _focusedControl = nextFocused;
            _focusedControl.internalSetFocus(true);
        }
    }

    void procPaste(const(char)[] text)
    {
        writeln("pasted: ", text);
    }

    // ----

    // Styles -----------------------------------------------------------------+
    void styleApplicatorWantAggregate(IstNode node, ref void* agg, out bool fromRefererence)
    {
        if (!node.info.value.length)
        {
            // this is either a reference that's not assigned or an
            // object without publication.
            return;
        }
        else if (node.info.value != cast(ubyte[])node.info.rtti.classInfo.identifier)
        {
            // reference: for non-reference the value is the type.
            fromRefererence = true;
        }
        else
        {
            Control c = cast(Control) iz.memory.factory(classesRepository, node.info.rtti.classInfo.identifier);
            if (c)
            {
                agg = cast(void*) c;
                PropDescriptor!Object* des = cast(PropDescriptor!Object*) node.info.descriptor;
                des.set(c);
                //TODO-cstyles: detect the parent of primitives created from the factory.
                addChild(c);
                //factory will not always create controls.
            }
            else writeln("agg not set: ", node.info.name );
        }
    }

    void styleApplicatorWantDescriptor(IstNode node, ref Ptr descriptor, out bool stop)
    {
        // find matching prop...

        if (node.info.value.length)
        {
            descriptor = publicationFromName(node.info.name[]);
            if (descriptor)
                writeln("found missing descr: ", node.info.name[]);
            else
                writeln("missing descr not found: ", node.info.name[]);
        }
    }

    /**
     * Virtual method called after that the style is applied.
     * It's designed to assign events to style elements.
     */
    void procAfterStyling()
    {
        // assign the events of the style elements using findPublisher...

        // if (auto f = findPublisher(this, "content.foreground"))
        //      (cast(Rectangle)f).onClick = ....
    }
    // ----

    // Misc. ------------------------------------------------------------------+

    /**
     * Virtual method called when an action is assigned and when it changes.
     * Should be used to bind the properties defined in the action.
     */
    void updateFromAction()
    {
        if (!_action)
            return;
        if (_action.caption.length)
            _caption = _action.caption;
        if (_action.hint.length)
            _hint = _action.hint;
    }

    void updateMatrix()
    {
        cairo_matrix_init_translate(&_matrix, parentedRect.left, parentedRect.top);
        if (_rotationAngle != 0)
        {
            const double x = _sizeRect.width * 0.5;
            const double y = _sizeRect.height * 0.5;
            cairo_matrix_translate(&_matrix, x, y);
            cairo_matrix_rotate(&_matrix, (Pi!2 / 360) * _rotationAngle);
            cairo_matrix_translate(&_matrix, -x, -y);
        }
        _imatrix = _matrix;
        cairo_matrix_invert(&_imatrix);
    }

    /**
     * If it's assigned, call the event onPaint.
     */
    final void doPaint(ref Canvas canvas)
    {
        if (_onPaint)
            _onPaint(this, _drawRect, canvas);
    }
    // ----

package:

    // Input dispatchers ------------------------------------------------------+

    static __gshared CustomControl _focusedControl;

    /**
     * Function template used by an orphan Control (usually an OsWindow) to
     * dispatch the mouse input to the children. The method is recursive so
     * that a child doesn't need to dispatch in procMouseDown(), procMouseUp()
     * or procMouseMove().
     */
    static bool dispatchMouse(string state)(CustomControl root, double x, double y,
        const ref MouseButtons mbs, const ref KeyModifiers kms, byte mwDelta)
    //if (state == "down" || state == "up" || state == "move" || state == "wheel")
    {

        bool result;
        const double ox = x;
        const double oy = y;

        auto loop(bool all = false)()
        {
            static if (all)
            {
                foreach(CustomControl child; root.children)
                {
                    if (child.wantMouse && child.visible)
                        dispatchMouse!(state)(child, x - root.left, y - root.top,
                            mbs, kms, mwDelta);
                }
            }
            else
            {
                foreach(CustomControl child; root.children)
                {
                    if (child.wantMouse && child.visible)
                        if (dispatchMouse!(state)(child, x - root.left, y - root.top,
                            mbs, kms, mwDelta))
                                return true;
                }
                return false;
            }
        }

        if (root.rotationAngle != 0)
        {
            const Point pt = root.transformPoint(x,y);
            x = pt.x;
            y = pt.y;
        }


        const bool inside = root.wantMouse && root.visible && root.isPointInside(x,y);

        if (inside && !root._penetrated)
            root.procMouseEnter;

        static if (state == "click" || state == "doubleclick")
        {
            if (inside)
            {
                static if (state == "click")
                    root.procClick;
                else
                    root.procDoubleClick;
                result = true;
            }
            loop!true;
        }

        static if (state == "down")
        {
            if (inside)
            {
                if (root._focusable)
                {
                    if (_focusedControl)
                        _focusedControl.internalSetFocus(false);
                    root.internalSetFocus(true);
                    _focusedControl = root;
                }
                if (MouseButton.left in mbs)
                    root._leftMbDown = true;
                root.procMouseDown(ox - root.left, oy - root.top, mbs, kms);
                result = true;

                if (loop!false)
                    return true;
            }
        }

        static if (state == "up")
        {
            if (inside || (root._leftMbDown & MouseButton.left !in mbs))
            {
                root._leftMbDown = false;
                root.procMouseUp(ox - root.left, oy - root.top, mbs, kms);
                loop!true;
            }
        }

        static if (state == "move")
        {
            root.procMouseMove(ox - root.left, oy - root.top, kms);
            loop!true;
        }

        static if (state == "wheel")
        {
            if (inside)
            {
                root.procMouseWheel(mwDelta, kms);
                loop!true;
            }
        }

        if (!inside)
        {
            if (root._penetrated)
                root.procMouseLeave;
            //if (root.focused && root._leftMbDown)
            //    root._leftMbDown = false;
        }

        return result;
    }

    /**
     * Function template used by an orphan Control (usually an OsWindow) to
     * dispatch the keyboard to the focused control.
     */
    static void dispatchKey(string state)(CustomControl root, dchar key,
        const ref KeyModifiers km)
    if (state == "down" || state == "up")
    {
        if (_focusedControl)
        {
            static if (state == "down")
                _focusedControl.procKeyDown(key, km);
            else static if (state == "up")
                _focusedControl.procKeyUp(key, km);
        }
    }

    // ----

public:

    // Standard Object things -------------------------------------------------+

    static this()
    {
        version (show_need_repaint)
        {
            _updateMark = construct!DrawableBitmap(10,10);
            _updateMark.canvas.fillBrush.color = ColorConstant.green;
            _updateMark.canvas.circle(5,5,5);
            _updateMark.canvas.fill!true;
        }
    }

    static ~this()
    {
        version (show_need_repaint)
        {
            destruct(_updateMark);
        }
    }

    /**
     * This is CustomControl default constructor.
     * If a control is created with `addControl` then the values of `caption`,
     * `left`, `top`, `width`, `height` and `alignment` that are set in this
     * constructor can be overwritten.
     */
    this()
    {
        _bmp = construct!DrawableBitmap;
        //_dynamicPublications = construct!DynPub;
        collectPublications!CustomControl;
        aligners = Aligners(&toNone, &toLeft, &toRight, &toTop, &toBottom, &toClient);

        beginRealign;
        wantKeys  = true;
        wantMouse = true;
        visible   = true;
        _focusable= true;
        left = 0;
        top = 0;
        width = 100;
        height = 60;
        _needRepaint = true;
        endRealign;
    }

    ~this()
    {
        destructEach(_caption, _hint, _styler, _bmp);
        deleteChildren;
        callInheritedDtor;
    }

    // ----

    // Alignment --------------------------------------------------------------+

    /**
     * Realigns recurisvely the children using their `alignment` property.
     */
    final void realignChildren()
    {
        Rect available = _sizeRect;
        foreach(CustomControl child; children)
        if (child.visible)
        {
            aligners[child.alignment](child, available);
        }
        updateDrawRect();
    }

    /**
     * By default calls `realignChildren()` then `customRealign()`.
     *
     * A Kheops control has two realignment methods. `RealignChildren()` applies
     * what's called the default alignment: it is simple, efficient and
     * non-overriddable. Derived classes, such as the layouts, can bypass the
     * first method and implement a custom realignment scheme in
     * `customRealignChildren()`.
     */
    void realign()
    {
        if (_realignLock)
            return;
        ++_realignLock;
        _realignCount = 0;
        realignChildren;
        customRealignChildren;
        if (_onRealigned)
            _onRealigned(this);
        --_realignLock;
    }

    /**
     * Increments the counter that prevents too many calls to Realign().
     * Should be used before changing several control properties.
     * Each call to beginRealign() must have a matching call to endRealing().
     */
    final void beginRealign()
    {
        ++_realignCount;
    }

    /**
     * Decrements the counter that saves too many calls to Realign().
     * The realignment happens when the counter goes back to 0.
     */
    final void endRealign()
    {
        --_realignCount;
        if (_realignCount <= 0)
            realign;
    }

    /**
     * Tries to realign immediatly. Only happens if the control is not already
     * realigning.
     */
    void tryRealign()
    {
        if (_realignCount <= 0)
            realign;
    }

    /**
     * Sets or gets the control alignment inside its parent.
     *
     * When set to none, left/right,top/bottom indicate the position and the
     * margin has no effect over the position.
     * When set to another value, left/right/top/bottom may be changed by the
     * parent control according to its available area and the margin is applied.
     */
    @Set void alignment(Alignment value)
    {
        if (_alignment == value)
            return;
        _alignment = value;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get Alignment alignment(){return _alignment;}

    /**
     * Sets or gets if the control is scaled. When the `alignment` is not set
     * to `none`, this property has for effect to maintain the ratio of the
     * size inside its parent. Note that when a control is scaled, its initial
     * size must be accurately defined since it's used to compute the initial
     * ratio, which is used during the whole control life-time.
     */
    @Set void scaled(bool value)
    {
        _scaled = value;
        _scalingRatio.x = 1;
        _scalingRatio.y = 1;
    }

    /// ditto
    @Get bool scaled() {return _scaled;}
    // ----

    // Position ---------------------------------------------------------------+

    /**
     * Returns true if a point (in the parented coordinate system) is inside the
     * control.
     *
     * By default it uses the control rect but the descendants can override the
     * method to test for insideness based on the path that defines the visual
     * control shape.
     */
    bool isPointInside(double x, double y)
    {
        return x >= _parentedRect.left && x <= _parentedRect.left + _parentedRect.width
            && y >= _parentedRect.top  && y <= _parentedRect.top + _parentedRect.height;
    }

    /**
     * Applies the control transformation to a point.
     *
     * Params:
     *      x = The point x coordinate, relative to the parent.
     *      y = The point y coordinate, relative to the parent.
     * Returns:
     *      A new point.
     */
    Point transformPoint(double x, double y)
    {
        cairo_matrix_transform_point(&_imatrix, &x, &y);
        x += _parentedRect.left;
        y += _parentedRect.top;
        return Point(x, y);
    }

    /**
     * Returns the control size as a `Rect`.
     * To preserve the alignment, this rect is read-only.
     */
    ref const(Rect) sizeRect() {return _sizeRect;}


    /**
     * Returns the control rect inside its parent.
     * To preserve the alignment, this rect is read-only.
     */
    ref const(Rect) parentedRect() {return _parentedRect;}

    /**
     * Returns the control width and height, directly usable in
     * a function call with `sizeList[0..$]`.
     */
    Tuple!(double,double) sizeList()
    {
        return tuple(_sizeRect.width, _sizeRect.height);
    }

    /**
     * Returns the control left and top, directly usable in
     * a function call with `positionList[0..$]`.
     */
    Tuple!(double,double) positionList()
    {
        return tuple(_parentedRect.left, _parentedRect.top);
    }

    /**
     * Returns the control coordinates and size,
     * directly usable in a function call with `sizeRectList[0..$]`.
     */
    Tuple!(double,double,double,double) sizeRectList()
    {
        return tuple(0.0, 0.0, _sizeRect.width, _sizeRect.height);
    }

    /**
     * Returns the control drawing area coordinates and size,
     * directly usable in a function call with `drawRectList[0..$]`.
     */
    Tuple!(double,double,double,double) drawRectList()
    {
        return tuple(_drawRect.left, _drawRect.top, _drawRect.width, _drawRect.height);
    }

    /**
     * Returns the control coordinates and size in its parent,
     * directly usable in a function call with `parentedRectList[0..$]`.
     */
    Tuple!(double,double,double,double) parentedRectList()
    {
        return tuple(_parentedRect.left, _parentedRect.top, _parentedRect.width,
            _parentedRect.height);
    }

    /**
     * Sets the control size.
     */
    void size(double w, double h)
    {
        w = max(1, w);
        h = max(1, h);
        updateScalingRatio(w, h);
        _sizeRect.width = w;
        _sizeRect.height = h;
        _parentedRect.width = w;
        _parentedRect.height = h;
        _bmp.size(cast(int) (w + 0.5), cast(int) (h + 0.5));
        updateMatrix;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /**
     * Sets the control position.
     */
    void position(double x, double y)
    {
        _parentedRect.left = x;
        _parentedRect.top = y;
        updateMatrix;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /**
     * Sets the control size and position.
     */
    void sizePosition(double x, double y, double w, double h)
    {
        w = max(1, w);
        h = max(1, h);
        updateScalingRatio(w, h);
        _sizeRect.width = w;
        _sizeRect.height = h;
        _parentedRect.left = x;
        _parentedRect.top = y;
        _parentedRect.width = w;
        _parentedRect.height = w;
        _bmp.size(cast(int) (w + 0.5), cast(int) (h + 0.5));
        updateMatrix;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /**
     * Sets or gets the control width.
     *
     * Depending on the alignment, this value may be changed by the parent.
     * The setter tries to realign.
     */
    @Set void width(double value)
    {
        value = max(1, value);
        if (_parentedRect.width == value)
            return;
        updateScalingRatio(value, _sizeRect.height);
        _parentedRect.width = value;
        _sizeRect.width = value;
        _bmp.width = cast(int) (value + 0.5);
        updateMatrix;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double width(){return _parentedRect.width;}

    /**
     * Sets or gets the control height.
     *
     * Depending on the alignment, this value may be changed by the parent.
     * The setter tries to realign.
     */
    @Set void height(double value)
    {
        value = max(1, value);
        if (_parentedRect.height == value)
            return;
        updateScalingRatio(_sizeRect.width, value);
        _parentedRect.height = value;
        _sizeRect.height = value;
        _bmp.height = cast(int) (value + 0.5);
        updateMatrix;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double height(){return _parentedRect.height;}

    /**
     * Sets or gets the control left position.
     *
     * Depending on the alignment, this value may be changed by the parent.
     * The setter tries to realign.
     */
    @Set void left(double value)
    {
        if (_parentedRect.left == value)
            return;
        _parentedRect.left = value;
        updateMatrix;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double left(){return _parentedRect.left;}

    /**
     * Sets or gets the control top position.
     *
     * Depending on the alignment, this value may be changed by the parent.
     * The setter tries to realign.
     */
    @Set void top(double value)
    {
        if (_parentedRect.top == value)
            return;
        _parentedRect.top = value;
        updateMatrix;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double top(){return _parentedRect.top;}

    /**
     * Sets the margin for all the sides of the control.
     */
    void marginAll(double value)
    {
        beginRealign;
        _margin = Margin(value, value, value, value);
        _needRepaint = true;
        endRealign;
    }

    /**
     * Sets or gets the control left margin.
     *
     * This value has only an effect when the control alignment is not equal
     * to `Alignment.none`.
     */
    @Set void marginLeft(double value)
    {
        if (_margin.left == value)
            return;
        _margin.left = value;
        if (alignment == Alignment.none)
            return;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double marginLeft(){return _margin.left;}

    /**
     * Sets or gets the control top margin.
     *
     * This value has only an effect when the control alignment is not equal
     * to `Alignment.none`.
     */
    @Set void marginTop(double value)
    {
        if (_margin.top == value)
            return;
        _margin.top = value;
        if (alignment == Alignment.none)
            return;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double marginTop(){return _margin.top;}

    /**
     * Sets or gets the control right margin.
     *
     * This value has only an effect when the control alignment is not equal
     * to `Alignment.none`.
     */
    @Set void marginRight(double value)
    {
        if (_margin.right == value)
            return;
        _margin.right = value;
        if (alignment == Alignment.none)
            return;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double marginRight(){return _margin.right;}

    /**
     * Sets or gets the control bottom margin.
     *
     * This value has only an effect when the control alignment is not equal
     * to `Alignment.none`.
     */
    @Set void marginBottom(double value)
    {
        if (_margin.bottom == value)
            return;
        _margin.bottom = value;
        if (alignment == Alignment.none)
            return;
        _needRepaint = true;
        if (!parent) tryRealign;
        else parent.tryRealign;
    }

    /// ditto
    @Get double marginBottom(){return _margin.bottom;}

    /**
     * Sets the padding for all the sides of the control.
     *
     * The padding defines the inner Rect passed when the control is painted
     * and it should be used to handle the stroke width.
     */
    void paddingAll(double value)
    {
        _padding = Margin(value, value, value, value);
        updateDrawRect();
        _needRepaint = true;
    }

    /**
     * Sets or gets the control left padding.
     *
     * The padding defines the inner Rect passed when the control is painted
     * and it should be used to handle the stroke width.
     */
    @Set void paddingLeft(double value)
    {
        if (_padding.left == value)
            return;
        _padding.left = value;
        updateDrawRect();
        _needRepaint = true;
    }

    /// ditto
    @Get double paddingLeft(){return _padding.left;}

    /**
     * Sets or gets the control top padding.
     *
     * The padding defines the inner Rect passed when the control is painted
     * and it should be used to handle the stroke width.
     */
    @Set void paddingTop(double value)
    {
        if (_padding.top == value)
            return;
        _padding.top = value;
        updateDrawRect();
        _needRepaint = true;
    }

    /// ditto
    @Get double paddingTop(){return _padding.top;}

    /**
     * Sets or gets the control right padding.
     *
     * The padding defines the inner Rect passed when the control is painted
     * and it should be used to handle the stroke width.
     */
    @Set void paddingRight(double value)
    {
        if (_padding.right == value)
            return;
        _padding.right = value;
        updateDrawRect();
        _needRepaint = true;
    }

    /// ditto
    @Get double paddingRight(){return _padding.right;}

    /**
     * Sets or gets the control bottom padding.
     *
     * The padding defines the inner Rect passed when the control is painted
     * and it should be used to handle the stroke width.
     */
    @Set void paddingBottom(double value)
    {
        if (_padding.bottom == value)
            return;
        _padding.bottom = value;
        updateDrawRect();
        _needRepaint = true;
    }

    /// ditto
    @Get double paddingBottom(){return _padding.bottom;}

    /**
     * Sets or gets the control rotation angle.
     */
    @Set void rotationAngle(double value)
    {
        if (_rotationAngle == value)
            return;
        _rotationAngle = value;
        updateMatrix;
        _needRepaint = true;
    }

    /// ditto
    @Get double rotationAngle() {return _rotationAngle;}

    /**
     * Translates the coordinates of a point relative to the client to a point
     * relative to the screen.
     */
    Point toWindowCoordinates(Point value)
    {
        value.x += left;
        value.y += top;
        auto prt = _parent;
        while (prt)
        {
            value.x += prt.left;
            value.y += prt.top;
            prt = prt._parent;
        }
        return value;
    }

    // ----

    // Visibility -------------------------------------------------------------+

    /**
     * Sets or gets the control visibility.
     *
     * Related:
     *      `procShow()` and `procHide()`, the virtual methods used
     *      to perform additional updates when the visibility changes.
     */
    @Set final void visible(bool value){setVisible(value);}

    /// ditto
    @Get final bool visible(){return _visible;}

    /**
     * Sets or gets the event called when the control visibility changes.
     */
    @Set final void onVisibilityChanged(Event value){_onVisibilityChanged = value;}

    /// ditto
    @Get final Event onVisibilityChanged(){return _onVisibilityChanged;}

    /**
     * Indicates wether the control has the focus.
     * Related:
     *      setFocus() and unsetFocus(), the methods that are called when the
     *      focus is gained or lost.
     */
    final bool focused() {return _focused;}

    /**
     * Indicates wether the control can has the focus.
     *
     * The primitives usually don't while their container does.
     */
    @Set void focusable(bool value) {_focusable = value;}

    /// ditto
    @Get bool focusable() {return _focusable;}

    /**
     * Sets or gets the order of the control when focused with the TAB key.
     *
     * `0` indicates that the tree order is followed.
     * A negative number indicates that the control cannot get the focus from
     * the TAB key.
     */
    @Set void tabOrder(int value)
    {
        //if (value > siblingCount)
        //    value = 0;
        _tabOrder = value;
    }

    /// ditto
    @Get int tabOrder() {return _tabOrder;}

    /**
     * Sets or gets the control index.
     *
     * The index defines the painting order but also the Z-order when alignment
     * is set to `Alignment.client`. Invalid values will be clamped. During
     * the deserialization, this value is never applied directly.
     */
    @Set void index(int value)
    {
        if (!parent._restoreIndex)
        {
            _index = value;
            _index = max(0, _index);
            _index = min(siblingCount - 1, _index);
            siblingIndex = _index;
        }
        else _index = value;
    }

    /// ditto
    @Get int index()
    {
        return (parent && parent._restoreIndex) ?
            _index : cast(int) siblingIndex;
    }

    /**
     * Applies the index property of each child.
     */
    final void applyChildrenIndexes()
    {
        import std.algorithm.sorting;

        CustomControl[]  ccs;
        foreach(child; children)
            ccs ~= child;

        auto result = sort!"a.index < b.index"(ccs);

        beginRealign;
        removeChildren;
        foreach(p; result)
            addChild(p);
        endRealign;

        _restoreIndex = false;
    }

    /**
     * Brings the control to the front, i.e over all the other controls
     * in the parent.
     */
    void toFront()
    {
        assert(parent);
        if (nextSibling)
            siblingIndex = nextSibling.siblingCount - 1;
        assert(!nextSibling);
        assert(parent);
    }

    /**
     * Sends the control to the back, i.e under all the other controls
     * in the parent.
     */
    void toBack()
    {
        if (prevSibling)
            siblingIndex = 0;
        assert(!prevSibling);
    }
    // ----

    // Mouse ------------------------------------------------------------------+

    /**
     * Sets or gets wether the control wants the mouse input.
     * Note that this property is not transitive.
     */
    @Set final void wantMouse(bool value){_wantMouse = value;}
    /// ditto
    @Get final bool wantMouse(){return _wantMouse;}

    /**
     * Sets or gets the event called when the mouse enters a control.
     *
     * Related:
     *      procMouseEnter(), virtual method called when the mouse enters
     *      a control.
     */
    @Set final void onMouseEnter(Event value){_onMouseEnter = value;}

    /// ditto
    @Get final Event onMouseEnter(){return _onMouseEnter;}

    /**
     * Sets or gets the event called when the mouse leaves a control.
     *
     * Related:
     *      procMouseLeave(), virtual method called when the mouse leaves
     *      a control.
     */
    @Set final void onMouseLeave(Event value){_onMouseLeave = value;}

    /// ditto
    @Get final Event onMouseLeave(){return _onMouseLeave;}

    /**
     * Sets or gets the event called when a mouse button is pressed.
     *
     * Related:
     *      procMouseDown(), virtual method called when a mouse button is
     *      pressed on a control.
     */
    @Set final void onMouseDown(MouseDownEvent value){_onMouseDown = value;}

    /// ditto
    @Get final MouseDownEvent onMouseDown(){return _onMouseDown;}

    /**
     * Sets or gets the event called when a mouse button is released.
     *
     * Related:
     *      procMouseUp(), virtual method called when a mouse button is
     *      released on the control.
     */
    @Set final void onMouseUp(MouseUpEvent value){_onMouseUp = value;}

    /// ditto
    @Get final MouseUpEvent onMouseUp(){return _onMouseUp;}


    /**
     * Sets or gets the event called when the mouse moves over the control.
     *
     * Related:
     *      procMouseMove(), virtual method called when the mouse moves
     *      over the control.
     */
    @Set final void onMouseMove(MouseMoveEvent value){_onMouseMove = value;}

    /// ditto
    @Get final MouseMoveEvent onMouseMove(){return _onMouseMove;}

    /**
     * Sets or gets the event called when the mouse wheel is used over a control.
     *
     * Related:
     *      procMouseWheel(), virtual method called when the mouse wheel is
     *      used over a control.
     */
    @Set final void onMouseWheel(MouseWheelEvent value){_onMouseWheel = value;}

    /// ditto
    @Get final MouseWheelEvent onMouseWheel(){return _onMouseWheel;}

    /**
     * Sets or gets the event called after a left mouse button click
     *
     * Related:
     *      procClick(), virtual method called after a left mouse button click
     */
    @Set final void onClick(Event value){_onClick = value;}

    /// ditto
    @Get final Event onClick(){return _onClick;}


    /**
     * Sets or gets the event called after a double left mouse button click
     *
     * Related:
     *      procDoubleClick(), virtual method called after a double left mouse button click
     */
    @Set final void onDoubleClick(Event value){_onDoubleClick = value;}

    /// ditto
    @Get final Event onDoubleClick(){return _onDoubleClick;}

    // ----

    // Keyboard ---------------------------------------------------------------+

    /**
     * Sets or gets wether the control wants the keyboard input.
     * Note that this property is not transitive.
     */
    @Set final void wantKeys(bool value){_wantKeys = value;}
    /// ditto
    @Get final bool wantKeys(){return _wantKeys;}

    /**
     * Sets or gets the event called when a key is pressed.
     *
     * Related:
     *      procKeyDown(), virtual method called when a key is pressed.
     */
    @Set final void onKeyDown(KeyDownEvent value){_onKeyDown = value;}

    /// ditto
    @Get final KeyDownEvent onKeyDown(){return _onKeyDown;}

    /**
     * Sets or gets the event called when a key is released.
     *
     * Related:
     *      procKeyUp(), virtual method called when a key is released.
     */
    @Set final void onKeyUp(KeyUpEvent value){_onKeyUp = value;}

    /// ditto
    @Get final KeyUpEvent onKeyUp(){return _onKeyUp;}

    // ----

    // Drawing ----------------------------------------------------------------+

    /**
     * Sets or gets if the control needs to be repaint.
     */
    void needRepaint(bool value) {_needRepaint = value;}

    /// ditto
    bool needRepaint(){return _needRepaint;}

    /**
     * Returns the inner rectangle in wihich the control should be paint.
     */
    ref const(Rect) drawRect() {return _drawRect;}

    /**
     * The control paints itself on a canvas.
     *
     * When this method is called, the canvas is already translated to match
     * to the control absolute position in its window.
     * This method is usually called by an OsWindow but may also be used to
     * render the control to a bitmap.
     */
    void paint(ref Canvas canvas)
    {
        _needRepaint = false;
        _bmp.clear;
        doPaint(canvas);
    }

    /**
     * Repaints from the root control.
     */
    void repaint()
    {
        // in the __ctor parent is not yet set
        if (_parent)
        {
            if (auto r = root)
                r.repaint;
        }
    }

    /**
     * Forces a full redrawing during the next repaint.
     *
     * Params:
     *      apply = When set to true, $(D repaint()) is called immediatly.
     */
    void invalidate(bool apply = false)
    {
        iterateTreeItems!((a) => a._needRepaint = true)(this);
        if (apply)
            repaint();
    }

    /**
     * The bitmap that encapsulates the drawing context and the surface.
     */
    DrawableBitmap bmp() {return _bmp;}

    /**
     * Sets and gets the event called when the control paints itself.
     */
    @Set final void onPaint(PaintEvent value){_onPaint = value;}

    /// ditto
    @Get final PaintEvent onPaint(){return _onPaint;}
    // ----

    // Style ------------------------------------------------------------------+

    /**
     * Applies the style using styleID
     */
    final void applyStyle()
    {
        //if (!stylesStock || !_styler.length)
        //    return;
        //
        //// get the style as a branch of controls in the stok
        //CustomControl stylerInstance = stylesStock.findStyle(_styler[]);
        //if (!stylerInstance)
        //    return;
        //
        //MemoryStream str = construct!MemoryStream;
        //Serializer ser = construct!Serializer;
        //scope(exit) destructEach(str, ser);
        //
        //// Serialize from the stock and deserialize in the control
        //ser.publisherToStream(stylerInstance, str);
        //str.position = 0;
        //applyStyle(str);
    }

    /**
     * Applies the style from an arbitrary stream.
     */
    final void applyStyle(Stream str)
    {
        Serializer ser = construct!Serializer;
        scope(exit) destruct(ser);
        _restoreIndex = true;

        // Serialize from the stock and deserialize in the control
        ser.onWantAggregate = &styleApplicatorWantAggregate;
        ser.onWantDescriptor = &styleApplicatorWantDescriptor;
        ser.streamToPublisher(str, this);

        applyChildrenIndexes;
        procAfterStyling;
    }

    /**
     * Sets or gets the ID of the style to apply. This must matches to
     * a Style.styleID value.
     */
    @Set void stylerID(string value)
    {
        if (_styler == value)
            return;
        _styler = value;
        applyStyle;
    }

    /// ditto
    @Get string stylerID() {return _styler[];}

    // ----

    // Misc. ------------------------------------------------------------------+

    /**
     * Constructs and adds a new child control in this control.
     *
     * Params:
     *      C = The type of the control.
     *      position = Its position inside the control.
     *      al = its alignment.
     *
     * Returns:
     *      A new C whose lifetime is managed by this control.
     */
    C addControl(C)(Rect position, Alignment al = Alignment.none,
        string capt = "")
    if (is (C : Control))
    {
        beginRealign;
        C result = construct!C;
        result.left = position.left;
        result.height = position.height;
        result.top = position.top;
        result.width = position.width;
        if (capt.length)
            result.caption = capt;
        result.alignment = al;
        addChild(result);
        endRealign;
        return result;
    }

    /**
     * Constructs and adds a new child control, aligned to the client.
     */
    C addClientControl(C)()
    {
        return addControl!C(Rect(0,0,1,1), Alignment.client);
    }

    /**
     * Sets of gets the control caption.
     */
    @Set void caption(string value)
    {
        _caption = value;
    }

    /// ditto
    @Get string caption() {return _caption[];}

    /**
     * Sets of gets the control hint.
     */
    @Set void hint(string value)
    {
        _hint = value;
    }

    /// ditto
    @Get string hint() {return _hint[];}

    /**
     * Sets or gets if the visual of the children is boxed by the rectangle
     * formed by this control.
     */
    @Set void clipChildren(bool value)
    {
        if (_clipChildren == value)
            return;

        _clipChildren = value;
    }

    /// ditto
    @Get bool clipChildren() {return _clipChildren;}

    /**
     * Sets or gets the action associated to this control.
     *
     * The usage of this property depends on the derived Control.
     * By default only the action caption is used.
     *
     * The action is not owned by the control so only its reference is serialized.
     * The action must be serialized elsewhere and it has to be registered in
     * the `ReferenceMan`.
     */
    @Set void action(Action value)
    {
        if (_action == value)
            return;
        if (value && _action)
            _action.onChanged = null;
        _action = value;
        _action.onChanged = &handleActionChanged;
        updateFromAction;
    }

    /// ditto
    @Get Action action() {return _action;}


    /**
     * Sets or gets the context menu associated to this control.
     *
     * The context menu is not owned by the control so only its reference is
     * serialized. The content of the menu must be serialized elsewhere and it
     * has to be registered in the `ReferenceMan`.
     */
    @Set void contextMenu(CustomMenuWindow value)
    {
        if (_contextMenu == value)
            return;
        _contextMenu = value;
    }

    /// ditto
    @Get CustomMenuWindow contextMenu() {return _contextMenu;}

    /**
     * Returns either the control's matrix or its inverse.
     */
    cairo_matrix_t* matrix(bool inverse = false)()
    {
        static if (inverse)
            return &_imatrix;
        else
            return &_matrix;
    }

    /**
     * Sets or gets if the control is serialized by its parent.
     */
    void published(bool value)
    {
        //if (value && parent && !published)
        //    parent._dynamicPublications.addItem(this);
    }

    /// ditto
    bool published()
    {
        return true;
        //import std.algorithm.searching: canFind;
        //if (!parent)
        //    return false;
        //else
        //    return parent._dynamicPublications.items.canFind(this);
    }

    ///// Sets and gets the array used to dynamically add published controls.
    //@Set dynamicPublications(DynPub){}
    //
    ///// ditto
    //@Get DynPub dynamicPublications(){return _dynamicPublications;}

    /// Sets or gets if the control can be designed.
    @Set void designable(bool value)
    {
        _designable = value;
    }

    /// ditto
    @Get bool designable(){return _designable;}

    /**
     * Sets or gets the event called after the focused changed.
     */
    @Set final void onFocusedChanged(Event value){_onFocusedChanged = value;}

    /// ditto
    @Get final Event onFocusedChanged(){return _onFocusedChanged;}

    /**
     * Sets or gets the event called after the realignment applied.
     */
    @Set final void onRealigned(Event value){_onRealigned = value;}

    /// ditto
    @Get final Event onRealigned(){return _onRealigned;}
    // ----
}


/**
 * Deept iterator specialized for the control tree structure.
 *
 * Params:
 *      Fun = a delegate literal, a static function or a lambda called for each
 *      child. The first parameter has to be of type CustomControl. The next
 *      parameters are described by a a the variadic A.
 */
bool iterateTreeItems(alias Fun, alias Pred = null, A...)(CustomControl root, A a)
{
    enum hasPred = is(Pred) && is(typeof(Pred));

    bool result = true;
    Fun(root, a);

    static if (hasPred)
        if (Pred(a))
            return false;

    foreach(child; root.children)
    {
        static if (!hasPred)
            result = iterateTreeItems!(Fun, Pred)(child, a);
        else
        {
            result = iterateTreeItems!(Fun, Pred)(child, a);
            if (!result)
                return false;
        }
    }

    return result;
}


/**
 * The Mouse structure allows to get and set the mouse position in a
 * specific OsWindow.
 *
 * An instance is automatically created for each new OsWindow.
 */
struct Mouse
{
    private OsWindowHandle _winHdl;

    @disable this(this);

    /// Constructs an instance with the handle of the target window.
    this(OsWindowHandle winHdl)
    {
        _winHdl = winHdl;
    }

    /// Returns the mouse position as a point of int.
    PointI32 position() const
    {
        PointI32 result;
        version(linux)
        {
            int a,b;
            uint c;
            Window r,t;
            XQueryPointer(display, _winHdl, &r, &t, &a, &b,
                &result.x, &result.y, &c);
            return result;
        }
        else static assert(0);
    }

    /// Sets the mouse position from a point.
    void position(T)(PointOf!T value) const
    if (isNumeric!T)
    {
        version(linux)
        {
            XWarpPointer(display, _winHdl, _winHdl, 0, 0, 0, 0,
                cast(int) value.x, cast(int) value.y);
        }
        else static assert(0);
    }

    /// Simulates a click event
    void click(const MouseButtons btns = [MouseButton.left]) const
    {
        version(linux)
        {
            XEvent pressEvent;
            pressEvent.type = ButtonPress;
            const PointI32 pt = position;
            pressEvent.xbutton.display = display;
            pressEvent.xbutton.window = _winHdl;
            pressEvent.xbutton.x = pt.x;
            pressEvent.xbutton.y = pt.y;
            pressEvent.xbutton.button = btns.container + 1;

            XEvent releaseEvent = pressEvent;
            releaseEvent.type = ButtonRelease;

            XSendEvent(display, _winHdl, false, 0, &pressEvent);
            XSendEvent(display, _winHdl, false, 0, &releaseEvent);
        }
        else static assert(0);
    }

    /// Simulates a mouse wheel event
    void wheel(bool down = false) const
    {
        version(linux)
        {
            XEvent event;
            event.type = ButtonPress;
            const PointI32 pt = position;
            event.xbutton.display = display;
            event.xbutton.window = _winHdl;
            event.xbutton.x = pt.x;
            event.xbutton.y = pt.y;
            event.xbutton.button = down ? 5 : 4;

            XSendEvent(display, _winHdl, false, 0, &event);
        }
        else static assert(0);
    }
}


/**
 * The screens structure allows to retieve informations about the screens,
 * such as the count and their size.
 *
 * This is a static struct that can be used as is.
 */
struct screens
{
    @disable this();
    @disable this(this);

    /// Returns the count of screen
    static int count()
    {
        version(linux)
            return XScreenCount(display);
        else static assert(0);
    }

    /// size[index] returns the nth screen width and height as a PointI32
    struct size
    {
        version(linux)
        static PointI32 opIndex(size_t index)
        {
            version(X86_64)
                int _index = cast(int) index;
            else
                alias _index = index;
            return PointI32(
                XDisplayWidth(display, _index),
                XDisplayHeight(display, _index)
            );
        }
        else static assert(0);
    }
}


/**
 * The ancestor of all the high level controls.
 *
 * While most of the methods a control needs are implemented in
 * CustomControl, realignment of the children will not work properly
 * if used as parent of a compound control.
 */
class Control: CustomControl
{

    mixin inheritedDtor;

public:

    override void addChild(CustomControl child)
    {
        super.addChild(child);
        if (child)
        {
            child._design = _design;
            tryRealign;
        }
    }

    override CustomControl removeChild(size_t index)
    {
        CustomControl orphan = super.removeChild(index);
        if (orphan)
        {
            tryRealign;
            orphan._design = false;
        }
        return orphan;
    }

    override bool removeChild(CustomControl ctrl)
    {
        const bool result = super.removeChild(ctrl);
        if (result)
        {
            tryRealign;
            ctrl._design = false;
        }
        return result;
    }

    override void insertChild(CustomControl child)
    {
        super.insertChild(child);
        if (child)
        {
            tryRealign;
            child._design = _design;
        }
    }

    override void insertChild(size_t index, CustomControl child)
    {
        super.insertChild(index, child);
        if (child)
        {
            tryRealign;
            child._design = _design;
        }
    }
}


version(linux)
{
    import
        x11.X, x11.Xlib, x11.Xatom, x11.Xutil, cairo.x11;

    alias OsWindowHandle = Window;

    private enum WmProtocols
    {
        clipboard,
        clipbrd_sel_prim,
        clipbrd_format_text,
        deleteMessage,
        state,
        stateChange,
        maxH,
        maxV,
    }

    private __gshared Display* display;
    private __gshared Atom wmWindowType;
    private __gshared Atom[WmProtocols.max+1] wmProtocols;

    private enum NetWmState: int
    {
        remove = 0,
        add = 1,
        toggle = 2
    }

    static this()
    {
        display = XOpenDisplay(null);

        if(!display)
            throw new Error("Connection to X server failed");

        wmWindowType    = XInternAtom(display, "_NET_WM_WINDOW_TYPE", False);

        wmProtocols[WmProtocols.clipboard] =
            XInternAtom(display, "CLIPBOARD", False);
        wmProtocols[WmProtocols.clipbrd_format_text] =
            XInternAtom(display, "UTF8_STRING", False);
        wmProtocols[WmProtocols.clipbrd_sel_prim] =
            XInternAtom(display, "PRIMARY", False);

        wmProtocols[WmProtocols.deleteMessage] =
            XInternAtom(display, "WM_DELETE_WINDOW", False);
        wmProtocols[WmProtocols.state] =
            XInternAtom(display, "_NET_WM_STATE", False);
        wmProtocols[WmProtocols.stateChange] =
            XInternAtom(display, "WM_CHANGE_STATE", False);
        wmProtocols[WmProtocols.maxH] =
            XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
        wmProtocols[WmProtocols.maxV] =
            XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_VERT", False);
    }

    static ~this()
    {
        foreach(w; windows.byValue)
            destruct(w);
        destruct(windows);
    }
}


private __gshared HashMap_AB!(OsWindowHandle, OsWindow) windows;
private __gshared OsWindow* lastWindow;
private __gshared OsWindowHandle lastWindowHandle;


/**
 * Starts the kheops runtime and doesn't return until it's over.
 *
 * OsWindows are supposed to be created before calling this.
 * Params:
 *      controlRate = The approximative frequency, in milliseconds,
 *      at which the function checks for the runtime termination.
 */
void kheopsRun(ushort controlRate = 50)
{
    OsWindow.enterEventLoop;
    import core.thread;
    while (windows.count)
        Thread.sleep(dur!"msecs"(controlRate));
}


/**
 * Returns a pointer to the OsWindow matching to the native window handle.
 * Params:
 *      hdl = Under linux a X11.Window, under Windows a HWND.
 */
OsWindow* getOsWindow(OsWindowHandle hdl)
{
    if (hdl && hdl == lastWindowHandle)
        return lastWindow;
    else
    {
        auto result = hdl in windows;
        lastWindowHandle = hdl;
        lastWindow = result;
        return result;
    }
}


/**
 * The OsWindow (Os stand for Operating System) wraps the methods to create
 * a native system window and to listen to its events.
 *
 * An OsWindow is usually the "orphan control" of a GUI (the root) becasue it
 * manages the events, realignment and painting.
 */
class OsWindow: Control, Designer
{

    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    bool _decoration    = true;
    bool _icons         = true;
    bool _transparent   = false;
    WindowType _winType = WindowType.normal;
    WindowState _winState = WindowState.normal;
    WindowState _visibleState = WindowState.normal;
    Mouse _mouse;

    ShortcutEvent _onShortcut;
    dchar _chr;
    MouseButtons _mb;
    KeyModifiers _km;

    WindowCloseEvent _onClose;
    WindowCloseQueryEvent _onCloseQuery;

    @NoGc cairo_surface_t* _surfScreen;
    @NoGc cairo_surface_t* _surfBuffer;

    uint _wi = 600, _hi = 400;
    size_t _paintlock;
    double _mx = 0, _my = 0;

    version(linux)
    {
        int _screen;
        int _depth;
        @NoGc Visual* _visual;
        @NoGc cairo_t* _cr;
        @NoGc cairo_t* _sc;
        Window _win;
        int x11_fd;
        size_t _clickt0;
        static __gshared XButtonEvent prevMouseRelease;
    }

    void updateWindowProps()
    {
        struct MwmHints
        {
            uint flags;
            uint functions;
            uint decorations;
            uint input_mode;
            uint status;
        }

        enum
        {
            MWM_HINTS_FUNCTIONS   = 1,
            MWM_HINTS_DECORATIONS = 2,
            MWM_HINTS_INPUT_MODE  = 4,
            MWM_HINTS_STATUS      = 8,

            MWM_FUNC_ALL          = 1,
            MWM_FUNC_RESIZE       = 2,
            MWM_FUNC_MOVE         = 4,
            MWM_FUNC_MINIMIZE     = 8,
            MWM_FUNC_MAXIMIZE     = 16,
            MWM_FUNC_CLOSE        = 32,

            MWM_DECOR_ALL         = 1,
            MWM_DECOR_BORDER      = 2,
            MWM_DECOR_RESIZEH     = 4,
            MWM_DECOR_TITLE       = 8,
            MWM_DECOR_MENU        = 16,
            MWM_DECOR_MINIMIZE    = 32,
            MWM_DECOR_MAXIMIZE    = 64,
        }

        Atom mwmHintsProperty = XInternAtom(display, "_MOTIF_WM_HINTS", False);
        MwmHints hints;

        if (_decoration)
        {
            if (!_icons)
            {
                hints.flags =  MWM_HINTS_FUNCTIONS;
                hints.functions = MWM_FUNC_MINIMIZE | MWM_FUNC_CLOSE | MWM_FUNC_MOVE;
                hints.decorations = MWM_DECOR_RESIZEH | MWM_DECOR_BORDER;
            }
        }
        else
        {
            hints.flags = MWM_HINTS_DECORATIONS;
            hints.functions = MWM_FUNC_MOVE;
            hints.decorations = 0;
        }

        XChangeProperty(display, _win, mwmHintsProperty, mwmHintsProperty, 32,
            PropModeReplace, cast(ubyte*) &hints, 5);
    }

    void updateSurfaceSize()
    {
        if(!_surfScreen) return;

        version(linux)
        {
            cairo_xlib_surface_set_size(_surfScreen, _wi, _hi);
            if (_cr)
            {
                cairo_destroy(_cr);
                _cr = null;
            }
            if (_surfBuffer)
                cairo_surface_destroy(_surfBuffer);
            _surfBuffer = cairo_image_surface_create(CairoFormat.ARGB32, _wi, _hi);
            _needRepaint = true;
            dispatchPaint();
        }
        version(Win32) static assert(0);
    }

    void dispatchPaint()
    {
        if (!_surfScreen) return;
        if (_paintlock)
            return;
        _paintlock++;

        if (!_cr)
        {   _cr = cairo_create(_surfBuffer);
            _cv.setContext(_cr);
        }

        cairo_save(_cr);
        cairo_set_source_rgb(_cr, 1, 1, 1);
        cairo_set_operator (_cr, CairoOperator.Source);
        cairo_paint(_cr);
        cairo_restore(_cr);

        void paintChildren(CustomControl root)
        {
            if (!root._visible)
                return;
            _cv.beginControl(root);
            const bool rp = root._needRepaint;
            if (root._needRepaint)
            {
                root._bmp.canvas.scaleGradient(root);
                root.paint(root._bmp.canvas);
            }
            _cv.drawImage(root.sizeRect, root._bmp, Stretch.none);

            version (show_need_repaint)
            if (rp && _updateMark)
            {
                auto rc = Rect(0,0,10,10);
                _cv.drawImage(rc, root._updateMark, Stretch.none);
            }

            foreach(CustomControl child; root.children)
            {
                paintChildren(child);
            }
            _cv.endControl(root);
        }

        paint(_cv);
        foreach(child; children)
            paintChildren(child);

        cairo_set_source_rgb(_sc, 1, 1, 1);
        cairo_set_source_surface(_sc, _surfBuffer, 0 , 0);
        cairo_paint(_sc);

        _paintlock--;
    }

protected:

    ActionList _actionList;
    @NoGc Canvas _cv;

    /// Sets the window size from the event loop
    void sizeFromEvents(uint w, uint h)
    {
        this.size(w, h);
        updateSurfaceSize;
    }

    /// Sets the window position from the event loop
    void positionFromEvents(uint x, uint y)
    {
        this.position(x, y);
    }

    /// Process the action list and the onShortcut event
    void procShortcut()
    {
        foreach (Action act; _actionList)
        {
            if (act.shortcut.key == _chr.toUpper &&
                act.shortcut.key != _chr.init &&
                act.shortcut.keyModifiers == _km)
                act.execute;
        }
        if (_onShortcut)
            _onShortcut(this, _chr.toUpper, _km);
    }

    /// Process the onCloseQuery event and returns true if can close
    bool procCloseQuery(WindowCloseKind value)
    {
        bool result = true;
        if (_onCloseQuery)
            _onCloseQuery(this, value, result);
        return result;
    }

    /// Shows the window
    override void procShow()
    {
        version(linux)
        {
            if (display && _win)
                XMapWindow(display,_win);
        }
        else
            static assert(0);
    }

    /// Hides the window, without destroying it
    override void procHide()
    {
        version(linux)
        {
            if (procCloseQuery(WindowCloseKind.hide) && display && _win)
                XUnmapWindow(display, _win);
        }
        else
            static assert(0);
    }

    final void createWindow(int x = 10, int y = 10, int w = 800, int h = 600,
        OsWindowHandle prt = OsWindowHandle.init, bool doShow = true)
    {
        _wi = w;
        _hi = h;
        version(linux)
        {
            if (prt == OsWindowHandle.init)
                prt = XRootWindow(display,_screen);

            if (_cr)
                cairo_destroy(_cr);
            if (_sc)
                cairo_destroy(_cr);
            if (_surfBuffer)
                cairo_surface_destroy(_surfBuffer);
            if (_surfScreen)
                cairo_surface_destroy(_surfScreen);

            _win = XCreateWindow(display, prt, x, y, w, h, 5, _depth,  InputOutput, _visual, 0, null);
            _surfScreen = cairo_xlib_surface_create(display, _win, _visual, w, h);
            _surfBuffer = cairo_image_surface_create(CairoFormat.ARGB32, w, h);
            _sc = cairo_create(_surfScreen);

            _mouse = Mouse(_win);

            XSetWMProtocols(display, _win, wmProtocols.ptr, WmProtocols.max + 1);
            //XSetSelectionOwner(display, XA_PRIMARY, _win, CurrentTime);

            windows[_win] = this;

            XSelectInput(display, _win,
                KeyPressMask            |
                KeyReleaseMask          |
                ButtonPressMask         |
                ButtonReleaseMask       |
                EnterWindowMask         |
                LeaveWindowMask         |
                PointerMotionMask       |
                Button1MotionMask       |
                Button2MotionMask       |
                Button3MotionMask       |
                Button4MotionMask       |
                Button5MotionMask       |
                ButtonMotionMask        |
                KeymapStateMask         |
                ExposureMask            |
                VisibilityChangeMask    |
                StructureNotifyMask     |
                SubstructureRedirectMask|
                PropertyChangeMask      |
                OwnerGrabButtonMask
            );
        }
        else static assert(0);

        // by default Control sets visible in its ctor
        // but here it can't be applied
        visible = doShow;
        if (visible) procShow;
    }

public:

    this(int x = 10, int y = 10, int w = 800, int h = 600,
        OsWindowHandle prt = OsWindowHandle.init, bool doShow = true)
    {
        _actionList = construct!ActionList;
        collectPublications!OsWindow;

        internalSetFocus(true);

        version(linux)
        {
           _screen  = DefaultScreen(display);
           _visual  = DefaultVisual(display, _screen);
           _depth   = DefaultDepth(display, _screen);
        }
        else static assert(0);

        createWindow(x, y, w, h, prt, doShow);
    }

    ~this()
    {
        windows.remove(_win);
        destruct(_cv);
        if (_surfScreen)
            cairo_surface_destroy(_surfScreen);
        if (_surfBuffer)
            cairo_surface_destroy(_surfBuffer);
        if (_cr)
            cairo_destroy(_cr);
        if (_sc)
            cairo_destroy(_sc);
        destruct(_actionList);
        callInheritedDtor;
    }

    static void enterEventLoop()
    {
        version(linux)
        {
            XEvent event;
            KeySym  keysym;
            char[4] buffer;

            EVENT_LOOP: while(true)
            {
                while (XPending(display))
                {
                    XNextEvent(display, &event);

                    OsWindow* windowForEvent = getOsWindow(event.xany.window);
                    if (!windowForEvent)
                        continue;

                    with (*windowForEvent) switch(event.type)
                    {
                        default:
                            break;
                        case KeyPress:
                            const int i = XLookupString(&event.xkey, buffer.ptr, 4, &keysym, null);
                            // symbol
                            if (isVirtualKey(keysym)) switch (keysym)
                            {
                                case VirtualKey.VK_ALT_L, VirtualKey.VK_ALT_R:
                                    _km += KeyModifier.alt;
                                    goto default;
                                case VirtualKey.VK_CTRL_L, VirtualKey.VK_CTRL_R:
                                    _km += KeyModifier.ctrl;
                                    goto default;
                                case VirtualKey.VK_SHIFT_L, VirtualKey.VK_SHIFT_R:
                                    _km += KeyModifier.shift;
                                    goto default;
                                case VirtualKey.VK_META_L, VirtualKey.VK_META_R:
                                    _km += KeyModifier.meta;
                                    goto default;
                                default:
                                    _chr = cast(dchar) keysym;
                                    dispatchKey!"down"(*windowForEvent, _chr, _km);
                            }
                            // character
                            else if (i)
                            {
                                if (_km.none)
                                    _chr = cast(dchar) buffer[0];
                                else
                                {
                                    _chr = cast(dchar) keysym;
                                    procShortcut;
                                }
                                dispatchKey!"down"(*windowForEvent, _chr, _km);
                            }
                            break;
                        case KeyRelease:
                            const int i = XLookupString(&event.xkey, buffer.ptr, 4, &keysym, null);
                            // symbol
                            if (isVirtualKey(keysym)) switch (keysym)
                            {
                                case VirtualKey.VK_ALT_L, VirtualKey.VK_ALT_R:
                                    _km -= KeyModifier.alt;
                                    goto default;
                                case VirtualKey.VK_CTRL_L, VirtualKey.VK_CTRL_R:
                                    _km -= KeyModifier.ctrl;
                                    goto default;
                                case VirtualKey.VK_SHIFT_L, VirtualKey.VK_SHIFT_R:
                                    _km -= KeyModifier.shift;
                                    goto default;
                                case VirtualKey.VK_META_L, VirtualKey.VK_META_R:
                                    _km -= KeyModifier.meta;
                                    goto default;
                                default:
                                    _chr = cast(dchar) keysym;
                                    dispatchKey!"up"(*windowForEvent, _chr, _km);
                            }
                            // character
                            else if (i)
                            {
                                if (_km.none)
                                    _chr = cast(dchar) buffer[0];
                                else
                                    _chr = cast(dchar) keysym;
                                dispatchKey!"up"(*windowForEvent, _chr, _km);
                            }
                            break;
                        case ButtonPress:
                            _mx = event.xbutton.x;
                            _my = event.xbutton.y;
                            if (event.xbutton.button < 4)
                            {
                                _mb += cast(MouseButton) (event.xbutton.button - 1);
                                dispatchMouse!"down"(*windowForEvent, _mx + left, _my + top, _mb, _km, 0);
                            }
                            else if (event.xbutton.button < 6)
                            {
                                const byte wd = (event.xbutton.button == 4) ? 1 : -1;
                                dispatchMouse!"wheel"(*windowForEvent, _mx + left, _my + top, _mb, _km, wd);
                            }
                            break;
                        case ButtonRelease:
                            if (event.xbutton.button >= 4)
                                break;
                            // up
                            _mx = event.xbutton.x;
                            _my = event.xbutton.y;
                            _mb -= cast(MouseButton) (event.xbutton.button - 1);
                            dispatchMouse!"up"(*windowForEvent, _mx + left, _my + top, _mb, _km, 0);
                            // double click
                            bool dbl;
                            prevMouseRelease = event.xbutton;
                            if (event.xbutton.button == 1 && _clickt0 > 0 &&
                                event.xbutton.time <= _clickt0 + 200)
                            {
                                dbl = true;
                                prevMouseRelease.time = 0;
                            }
                            _clickt0 = event.xbutton.time;
                            if (dbl)
                                dispatchMouse!"doubleclick"(*windowForEvent, _mx + left, _my + top, _mb, _km, 0);
                            break;
                        case MotionNotify:
                            _mx = event.xmotion.x;
                            _my = event.xmotion.y;
                            dispatchMouse!"move"(*windowForEvent, _mx + left, _my + top, _mb, _km, 0);
                            break;
                        case EnterNotify:
                            _mx = event.xmotion.x;
                            _my = event.xmotion.y;
                            break;
                        case LeaveNotify:
                            _mx = event.xmotion.x;
                            _my = event.xmotion.y;
                            break;
                        case FocusIn:
                            internalSetFocus(true);
                            break;
                        case FocusOut:
                            internalSetFocus(false);
                            break;
                        case KeymapNotify:
                            break;
                        case Expose:
                            dispatchPaint;
                            break;
                        case GraphicsExpose:
                            break;
                        case NoExpose:
                            break;
                        case VisibilityNotify:
                            break;
                        case CreateNotify:
                            break;
                        case DestroyNotify:
                            break;
                        case UnmapNotify:
                            _winState = WindowState.minimized;
                            break;
                        case MapNotify:
                            _winState = _visibleState;
                            _km = 0;
                            break;
                        case MapRequest :
                            break;
                        case ReparentNotify:
                            break;
                        case ConfigureNotify:
                            if ((_wi < event.xconfigure.width || _hi < event.xconfigure.height) &&
                                event.xconfigure.width == event.xany.display.screens.width &&
                                event.xconfigure.height == event.xany.display.screens.height)
                            {
                                _winState = WindowState.maximized;
                                _visibleState = _winState;
                            }
                            _wi = event.xconfigure.width;
                            _hi = event.xconfigure.height;
                            sizeFromEvents(_wi, _hi);
                            dispatchPaint;
                            break;
                        case ConfigureRequest:
                            break;
                        case GravityNotify:
                            break;
                        case ResizeRequest:
                            break;
                        case PropertyNotify:
                            if (event.xproperty.atom == wmProtocols[WmProtocols.state])
                            {
                                // min/max events triggered from win deco comes here...
                                // min | max can be obtained with  ?
                                // smthg like "XGetAtomValue()"
                            }
                            break;
                        case ColormapNotify:
                            break;
                        case ClientMessage:
                            if (event.xclient.data.l[0] == wmProtocols[WmProtocols.deleteMessage])
                            {
                                WindowCloseKind kind = (windows.count < 2) ?
                                    WindowCloseKind.terminate : WindowCloseKind.close;
                                if (procCloseQuery(kind))
                                {
                                    destruct(*windowForEvent);
                                    XDestroyWindow(display, event.xany.window);
                                    if (kind == WindowCloseKind.terminate)
                                    {
                                        XCloseDisplay(display);
                                        break EVENT_LOOP;
                                    }
                                }
                            }
                            break;
                        case MappingNotify:
                            break;
                        case GenericEvent:
                            break;
                        case SelectionNotify:
                        if (_focusedControl)
                        //if (event.xselection.property == wmProtocols[WmProtocols.clipbrd_format_text])
                        {
                            /*ulong nitems, rem;
                            int format;
                            char* data;
                            Atom type;

                            XConvertSelection(display, wmProtocols[WmProtocols.clipboard], wmProtocols[WmProtocols.clipbrd_format_text], wmProtocols[WmProtocols.clipboard], windowForEvent.windowHandle, CurrentTime);

                            XGetWindowProperty(display, windowForEvent.windowHandle,
                                XA_PRIMARY, 0, 32, False, AnyPropertyType,
                                &type, &format, &nitems, &rem, cast(ubyte**)&data);

                            import core.stdc.string: strlen;
                            _focusedControl.procPaste(data[0..strlen(data)]);

                            if (data)
                                XFree(data);*/

                            break;
                        }
                    }
                }

                import kheops.timers: IdleTimer;
                import core.time: MonoTime;
                auto t = (MonoTime.currTime - MonoTime.zero).total!"msecs";
                IdleTimer.idleTick(t);
                if (prevMouseRelease.window && prevMouseRelease.time)
                {
                    OsWindow* windowForEvent = getOsWindow(prevMouseRelease.window);
                    with(windowForEvent) if (t > _clickt0 + 200)
                    {
                        prevMouseRelease.time = 0;
                        dispatchMouse!"click"(*windowForEvent, _mx + left, _my + top, _mb, _km, 0);
                    }
                }
            }
        }
        else
        {
            static assert(0);
        }
    }

    void wantPaste()
    {
        Window w = XGetSelectionOwner(display, wmProtocols[WmProtocols.clipbrd_sel_prim]);
        if (w)
        {
            XEvent ev;
            ev.type = SelectionRequest;
            ev.xany.display = display;
            ev.xany.window = _win;
            ev.xselectionrequest.requestor = windowHandle;
            ev.xselectionrequest.target = wmProtocols[WmProtocols.clipbrd_format_text];
            XSendEvent(display, windowHandle, false, 0, &ev);
        }
    }

    /**
     * Closes the window. When a single window remains, this method also
     * terminates the program.
     */
    void close()
    {
        version(linux)
        {
            XEvent ev;
            ev.type = ClientMessage;
            ev.xclient.display = display;
            ev.xclient.window = _win;
            ev.xclient.format = 32;
            ev.xclient.data.l[0] = wmProtocols[WmProtocols.deleteMessage];
            XSendEvent(display, _win, false, 0, &ev);
        }
        else
            static assert(0);
    }

    // Controls overrides -----------------------------------------------------+

    final override bool isPointInside(double x, double y)
    {
        return true;
    }

    final override void repaint() {dispatchPaint;}

    @Get bool showDecoration() {return _decoration;}

    @Set override void caption(string value)
    {
        _caption = value;
        version(linux)
        {
            import std.utf: toUTFz;
            XStoreName(display, _win, _caption[].toUTFz!(char*));
        }
        version(Win32)
        {
            static assert(0);
        }
    }

    @Get override string caption() {return super.caption();}

    @Set override void height(double value)
    {
        if (height == value) return;
        super.height(value);
        version(linux)
        {
            if (display && _win)
                XResizeWindow(display, _win, cast(uint) width(), cast(uint) height());
            updateSurfaceSize;
        }
        version(Win32) static assert(0);
    }

    @Get override double height() {return super.height();}

    @Set override void width(double value)
    {
        if (width == value) return;
        super.width(value);
        version(linux)
        {
            if (display && _win)
                XResizeWindow(display, _win, cast(uint) width(), cast(uint) height());
            updateSurfaceSize;
        }
        version(Win32) static assert(0);
    }

    @Get override double width() {return super.width();}

    @Set override void left(double value)
    {
        if (left == value) return;
        super.left(value);
        version(linux)
        {
            if(display && _win)
                XMoveWindow(display, _win, cast(uint) left, cast(uint) top);
        }
        version(Win32)
        {
            static assert(0);
        }
    }

    @Get override double left() {return super.left();}

    @Set override void top(double value)
    {
        if (top == value) return;
        super.top(value);
        version(linux)
        {
            if(display && _win)
                XMoveWindow(display, _win, cast(uint) left, cast(uint) top);
        }
        version(Win32)
        {
            static assert(0);
        }
    }

    @Get override double top() {return super.top();}

    // ----

    // Window specific properties ---------------------------------------------+

    @Set void transparent(bool value)
    {
        if (_transparent == value)
            return;
        _transparent = value;
        if (value)
        updateWindowProps;
    }

    @Get bool transparent() {return _transparent;}

    @Set void showIcons(bool value)
    {
        if (_icons == value)
            return;
        _icons = value;
        updateWindowProps;
    }

    @Get bool showIcons() {return _icons;}

    @Set void showDecoration(bool value)
    {
        if (_decoration == value)
            return;
        _decoration = value;
        updateWindowProps;
    }

    /**
     * Sets or gets the window type
     */
    @Set void windowType(WindowType value)
    {
        if (_winType == value)
            return;
        _winType = value;

        version(linux)
        {
            Atom data;
            with(WindowType) final switch(_winType)
            {
                case dialog:data = XInternAtom(display,
                    "_NET_WM_WINDOW_TYPE_DIALOG", False); break;
                case normal:data = XInternAtom(display,
                    "_NET_WM_WINDOW_TYPE_NORMAL", False); break;
                case splash:data = XInternAtom(display,
                    "_NET_WM_WINDOW_TYPE_SPLASH", False); break;
                case tool:  data = XInternAtom(display,
                    "_NET_WM_WINDOW_TYPE_UTILITY", False);break;

                /*"_NET_WM_WINDOW_TYPE_DESKTOP"
                "_NET_WM_WINDOW_TYPE_DOCK"
                "_NET_WM_WINDOW_TYPE_TOOLBAR"
                "_NET_WM_WINDOW_TYPE_MENU"
                "_NET_WM_WINDOW_TYPE_UTILITY"
                "_NET_WM_WINDOW_TYPE_SPLASH"
                "_NET_WM_WINDOW_TYPE_DIALOG"
                "_NET_WM_WINDOW_TYPE_NORMAL"*/

            }
            XChangeProperty(display, _win, wmWindowType, XA_ATOM, 32,
                PropModeReplace, cast(ubyte*) &data, 1);
        }
        version(Win32)
        {
            static assert(0);
        }
    }

    /// ditto
    @Get WindowType windowType(){return _winType;}


    /**
     * Sets or gets the window state.
     */
    @Set void windowState(WindowState value)
    {
        if (_winState == value)
            return;

        _winState = value;
        if (_winState != WindowState.minimized)
            _visibleState = _winState;

        XEvent ev;

        ev.type = ClientMessage;
        ev.xclient.display = display;
        ev.xclient.window = _win;
        ev.xclient.format = 32;
        ev.xclient.message_type = wmProtocols[WmProtocols.state];

        final switch (_winState)
        {
        case WindowState.normal:
            ev.xclient.data.l[0] = NetWmState.remove;
            ev.xclient.data.l[1] = wmProtocols[WmProtocols.maxH];
            ev.xclient.data.l[2] = wmProtocols[WmProtocols.maxV];
            ev.xclient.data.l[3..$] = 0;
            break;
        case WindowState.maximized:
            ev.xclient.data.l[0] = NetWmState.toggle;
            ev.xclient.data.l[1] = wmProtocols[WmProtocols.maxH];
            ev.xclient.data.l[2] = wmProtocols[WmProtocols.maxV];
            ev.xclient.data.l[3..$] = 0;
            break;
        case WindowState.minimized:
            ev.xclient.message_type = wmProtocols[WmProtocols.stateChange];
            ev.xclient.data.l[0] = IconicState;
        }
        XSendEvent(display, DefaultRootWindow(display), false,
            SubstructureNotifyMask | SubstructureRedirectMask, &ev);
    }

    /// ditto
    @Get WindowState windowState() {return _winState;}

    /**
     * Sets or gets the action list of this window.
     *
     * This list is owned and always serialized. The setter is a noop.
     * The action stored in this list can be assigned to the window's children
     * control and the shortcut will be automatically dispatched.
     */
    @Set void actionList(ActionList) {}

    ///
    @Get ActionList actionList() {return _actionList;}

    // ----

    // Window specific events -------------------------------------------------+

    /**
     * Sets or gets the event called when a shortcut happens.
     * This event is triggered after the dispatching to the action list.
     */
    @Set void onShortcut(ShortcutEvent value) {_onShortcut = value;}

    /// ditto
    @Get ShortcutEvent onShortcut() {return _onShortcut;}

    /**
     * Sets or gets the event called when a window is about to be closed.
     */
    @Set void onCloseQuery(WindowCloseQueryEvent value) {_onCloseQuery = value;}

    /// ditto
    @Get WindowCloseQueryEvent onCloseQuery() {return _onCloseQuery;}

    // ----

    // Read-only properties ---------------------------------------------------+

    /**
     * Returns the control that has the focus.
     *
     * It matches to the latest control that received a mouse button or
     * that was selected with a tab.
     */
    CustomControl focusedControl()
    {
        return _focusedControl;
    }

    /**
     * Returns the handle of the window, as a specific type of
     * the operating system or of a framework.
     */
    OsWindowHandle windowHandle()
    {
        version(linux) return _win;
        version(Win32) static assert(0);
    }

    /**
     * Returns the Mouse instance for this window.
     */
    ref const(Mouse) mouse() {return _mouse;}

    /**
     * Indicates wether a KeyModifier is down or not.
     */
    bool keyModifierDown(KeyModifier km)()
    {
        return km in _km;
    }
    // ----

    // Designer ---------------------------------------------------------------+

    /// Sets ot gets if design-time is enabled.
    @Set void design(bool value)
    {
        if (_design == value)
            return;
        _design = value;
        iterateTreeItems!((c) => c._design = _design)(this);
    }

    /// ditto
    @Get bool design() const
    {
        return _design;
    }
    // ----

}


/**
 * Elements of a CustomMenuWindow
 */
class MenuItem: Control
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Shortcut _shortcut;
    bool _overed;

protected:

    override void updateFromAction()
    {
        if (!_action)
            return;

        _caption = _action.caption;
        _hint = _action.hint;
        _shortcut = _action.shortcut;
    }

    override void procMouseEnter()
    {
        super.procMouseEnter();
        _overed = true;
        if (!isSeparator)
            repaint;
    }

    override void procMouseLeave()
    {
        super.procMouseLeave();
        _overed = false;
        if (!isSeparator)
            repaint;
    }

public:

    ///
    this()
    {
        super();
        wantMouse = true;
        collectPublications!MenuItem;
    }

    override void paint(ref Canvas canvas)
    {
        super.paint(canvas);
        if (isSeparator)
            return;
        if (!parent || ! cast(MenuItem) parent)
            return;

        const uint saved = canvas.fillBrush.color;

        if (_overed)
        {
            canvas.fillBrush.color = saved + 0x00202020;
            canvas.rectangle(sizeRectList[0..$]);
            canvas.fill!true;
        }

        canvas.fillBrush.color = ColorConstant.red;
        canvas.text(sizeRect, _caption[], Justify.left);
        canvas.fill!true;

        canvas.fillBrush.color = saved;
    }

    /**
     * Indicates wether the item is a separator.
     */
    bool isSeparator()
    {
        return _caption == "-";
    }

    /**
     * Sets of gets the item shortcut.
     */
    @Set void shortcut(Shortcut value)
    {
        _shortcut = value;
    }

    /// ditto
    @Get ref Shortcut shortcut() {return _shortcut;}
}


/**
 * Control used as context menu and as popup in the menu bar.
 */
class CustomMenuWindow: MenuItem
{

    mixin PropertyPublisherImpl;

private:

    kheops.font.Font _font;
    bool _ownItemsParent;

protected:

    override void unsetFocus()
    {
        toBack;
        visible = false;
        realign;
        repaint;
    }

    void updateSize()
    {
        Point sz;
        textInfo.setFont(_font);

        foreach (CustomControl cc; children)
        {
            if (!cc._visible)
                continue;

            double entryWidth, entryHeight;
            MenuItem mi = cast(MenuItem) cc;

            if (!mi)
                continue;

            mi.alignment = Alignment.none;
            mi.left = 0;
            mi.top = sz.y;

            if (_caption == "-")
                entryHeight = 3;
            else
            {
                TextExtent sz0 = textInfo(mi.caption);
                TextExtent sz1 = textInfo(mi.shortcut.text);

                entryWidth = sz0.width + sz1.width + 10;
                entryHeight = max(sz0.height, sz1.height) + 4;
            }

            if (sz.x < entryWidth)
                sz.x = entryWidth;

            sz.y += entryHeight;
            mi.height = entryHeight;

        }
        size(sz.x, sz.y);
        foreach (CustomControl cc; children)
            cc.width = sz.x;
    }

    void commonCtor(MenuItem itemsParent)
    {
        visible = false;
        _font = construct!(kheops.font.Font);
        collectPublications!CustomMenuWindow;
    }

public:

    ///
    this(MenuItem itemsParent)
    {
        super();
        commonCtor(itemsParent);
    }

    ///
    this()
    {
        super();
        commonCtor(null);
    }

    ~this()
    {
        destruct(_font);
        callInheritedDtor;
    }

    override void paint(ref Canvas canvas)
    {
        canvas.font = _font;
        super.paint(canvas);
        canvas.restoreDefaultFont;
    }

    /**
     * Shows the context menu at a particular position.
     */
    void show(double x, double y)
    {
        visible = true;
        if (_focusedControl)
            _focusedControl.internalSetFocus(false);
        _focusedControl = this;
        internalSetFocus(true);
        beginRealign;
        toFront;
        updateSize;
        position(x, y);
        endRealign;
        repaint;
    }

    /**
     * Closes the menu.
     */
    void close()
    {
        visible = false;
        if (_focusedControl is this)
        {
            _focusedControl.internalSetFocus(false);
            _focusedControl = null;
        }
    }

    /**
     * Adds an item to the menu.
     *
     * Params:
     *      caption = The item caption.
     *      action = The action for this item. When not null, the others parameters
     *      are ignored.
     */
    MenuItem addItem(string caption, Action action = null)
    {
        MenuItem result = addControl!MenuItem(Rect(0,0,1,1));
        if (action)
            result.action = action;
        else
            result.caption = caption;
        return result;
    }

    /**
     * Sets or gets the font used to draw the menu.
     */
    @Set void font(kheops.font.Font value)
    {
        if ( value)
            _font.copyFrom(value);
    }

    /// ditto
    @Get kheops.font.Font font() {return _font;}
}


/**
 * Interface for a class able to stylize a control.
 */
interface Stylist
{
    /// Recursively applies a style to the stylized controls
    void applyStyleFrom(Control value);
}


/**
 * Interface for a control that can be stylized by a Stylist.
 */
interface Stylized
{
    /// A sylist is about to apply the style.
    void beginStyling();
    /// A sylist has applied the style.
    void endStyling();
}


/**
 * Base class for the controls able to be stylized.
 */
class StylizedControl: Control, Stylized
{
    mixin inheritedDtor;

public

    /**
     * A sylist is about to apply the style.
     *
     * Used to backup the publications a styler may overwrite in the primitives.
     */
    void beginStyling(){}

    /**
     * A sylist has applied the style.
     *
     * Used to restore the publications a styler may overwrite in the primitives.
     */
    void endStyling(){}
}


unittest
{
    static assert(!MustAddGcRange!Control);
    static assert(!MustAddGcRange!OsWindow);
    static assert(!MustAddGcRange!MenuItem);
    static assert(!MustAddGcRange!CustomMenuWindow);
    static assert(!MustAddGcRange!StylizedControl);
    Control c = construct!Control;
    destruct(c);
}

