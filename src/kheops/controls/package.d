/**
 * Controls
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.controls;


import
    std.stdio, std.algorithm, std.conv;
import
    iz.memory, iz.properties;
import
    kheops.control, kheops.types, kheops.primitives, kheops.colors;



class Edit: StylizedControl
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    @NoGc RectangleEn _background;
    @NoGc Line _carret;
    @NoGc Text _text;

    size_t _carretPos;

    void setCaretPos(size_t value)
    {
        value = min(_text.caption.length, value);
        value = max(0, value);
        _carretPos = value;
        repaint;
    }

    void insertChar(dchar value)
    {
        _text.caption = _text.caption[0.._carretPos] ~ to!string(value) ~ _text.caption[_carretPos..$];
        setCaretPos(_carretPos + 1);
    }

    void deleteChar()
    {
        if (_text.caption.length && _carretPos > 0)
        {
            _text.caption = _text.caption[0.._carretPos-1] ~ _text.caption[_carretPos..$];
            setCaretPos(_carretPos - 1);
        }
    }

public:

    this()
    {
        super();
        _background = addClientControl!RectangleEn();
        _text = _background.addClientControl!Text();
        _carret = _text.addControl!Line(Rect(0,0,1,1));
        collectPublications!Edit();

        _background.clipChildren = true;
        _carret.kind = LineKind.leftToLeft;
    }

    override void procKeyDown(dchar key, const ref KeyModifiers km)
    {
        import kheops.helpers.keys;
        switch (key)
        {
        case '\r', '\n', '\v', '\f', '\t':
            break;
        case VirtualKey.VK_LEFT:
            setCaretPos(_carretPos - 1);
            break;
        case VirtualKey.VK_RIGHT:
            setCaretPos(_carretPos + 1);
            break;
        case VirtualKey.VK_BACKSPACE:
            deleteChar();
            break;
        default:
            if (!isVirtualKey(key))
                insertChar(key);
        }
    }

    /**
     * Sets or gets the background. For serialization, setter is a noop.
     */
    @Set void background(RectangleEn) {}

    /// ditto
    @Get RectangleEn background() {return _background;}

    /**
     * Sets or gets the text. For serialization, setter is a noop.
     */
    @Set void text(Text) {}

    /// ditto
    @Get Text text() {return _text;}

}

/**
 *
 */
class Label: StylizedControl
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    @NoGc RectangleEn _background;
    @NoGc TextEn _text;
    bool _autoSize;

protected:

public:

    this()
    {
        super();
        _background = addClientControl!RectangleEn();
        _text = _background.addClientControl!TextEn();
        collectPublications!Label();
    }

    override void endStyling()
    {
        if (_background)
            _background.alignment = Alignment.client;
        if (_text)
        {
            _text.alignment = Alignment.client;
            _text.caption = caption;
        }
    }

    alias caption = Control.caption;
    @Set override void caption(string value)
    {
        super.caption(value);
        if (_text)
            _text.caption = value;
    }

    /**
     * Sets or gets the background. For serialization, setter is a noop.
     */
    @Set void background(RectangleEn) {}

    /// ditto
    @Get RectangleEn background() {return _background;}

    /**
     * Sets or gets the text. For serialization, setter is a noop.
     */
    @Set void text(TextEn) {}

    /// ditto
    @Get TextEn text() {return _text;}

    /**
     * Sets or gets if the label size depends on the text.
     */
    @Set void autoSize(bool value)
    {
        if (_autoSize == value)
            return;
        _autoSize = value;
        beginRealign;
        if (_autoSize && _text)
        {
            repaint;
            size(_text.textWidth + 1, _text.textheight + 1);

        }
        endRealign;
    }

    /// ditto
    @Get bool autoSize() {return _autoSize;}
}


/**
 *
 */
class GroupBox: StylizedControl
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    @NoGc RectangleEn _background;
    @NoGc RectangleEn _backgroundFrame;
    @NoGc TextEn      _text;
    @NoGc Control     _content;

protected:

public:

    this()
    {
        super();
        _backgroundFrame = addClientControl!(RectangleEn);
        _text = _backgroundFrame.addControl!(TextEn)(Rect(0,0,1,24), Alignment.top);
        _content = _backgroundFrame.addClientControl!(Control);
        collectPublications!GroupBox();
    }

    override void endStyling()
    {
        if (_backgroundFrame && _text)
        {
            _backgroundFrame.alignment = Alignment.client;
            _backgroundFrame.paddingAll = _text.height * 0.5;
            _text.alignment = Alignment.top;
            _text.caption = caption;
            _text.toFront;
            _text.justify = Justify.center;
        }
        if (_content)
        {
            _content.marginLeft = _content.top;
            _content.marginRight = _content.top;
            _content.marginBottom = _content.top;
        }
    }

    alias caption = Control.caption;
    @Set override void caption(string value)
    {
        super.caption(value);
        if (_text)
            _text.caption = value;
    }

    /**
     * Sets or gets the group frame. For serialization, setter is a noop.
     */
    @Set void backgroundFrame(RectangleEn) {}

    /// ditto
    @Get RectangleEn backgroundFrame() {return _backgroundFrame;}

    /**
     * Sets or gets the text. For serialization, setter is a noop.
     */
    @Set void text(TextEn) {}

    /// ditto
    @Get TextEn text() {return _text;}

    /**
     * Returns the control that hosts the group elements.
     */
    Control content() {return _content;}

/*
    /// Redirect the operation to the content.
    override void addChild(CustomControl child)
    {
        if (_content && child)
            _content.addChild(child);
    }

    /// Redirect the operation to the content.
    override void insertChild(CustomControl child)
    {
        if (_content && child)
            _content.insertChild(child);
    }

    /// Redirect the operation to the content.
    override void insertChild(size_t index, CustomControl child)
    {
        if (_content && child)
            _content.insertChild(index, child);
    }*/
}

