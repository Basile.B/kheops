/**
 * Action
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.actions;

import
    iz.memory, iz.properties, iz.classes, iz.containers;
import
    kheops.helpers.keys, kheops.types;

static this()
{
    registerFactoryClasses!(Action, ActionList)(classesRepository);
}

/**
 * The Action class is a functor that stores additional informations so
 * that the event represented can be used by many controls, such as
 * buttons, menu items, a checkedboxes, etc.
 */
class Action: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Shortcut _shortcut1;
    Array!char _caption, _hint;
    bool _enabled = true;
    bool _checked;
    bool _autoCheck;
    Event _onExecute;
    Event _onChanged;

protected:

    enum doChanged =
    q{
        if (_onChanged)
            _onChanged(this);
    };

    /**
     * Virtual method called by `execute`.
     */
    void procExecute(){}

public:

    ///
    this()
    {
        collectPublications!Action;
    }

    ~this()
    {
        destructEach(_caption, _hint);
        destruct(_shortcut1);
        callInheritedDtor;
    }

    /**
     * If `enabled` then executes the action.
     * Also handles `checked` according to `autoCheck`.
     */
    final void execute()
    {
        if (!_enabled)
            return;
        procExecute;
        if (_onExecute)
            _onExecute(this);
        if (_autoCheck)
            _checked = !_checked;
    }

    /**
     * Sets or gets if the action is automatically checked.
     */
    @Set void autoCheck(bool value)
    {
        _autoCheck = value;
    }

    /// ditto
    @Get bool autoCheck()
    {
        return _autoCheck;
    }

    /**
     * Sets or gets if the action is enabled.
     */
    @Set void enabled(bool value)
    {
        if (_enabled == value)
            return;
        _enabled = value;
        mixin(doChanged);
    }

    /// ditto
    @Get bool enabled()
    {
        return _enabled;
    }

    /**
     * Sets or gets if the action is checked. This is used when
     * an action represents an option.
     */
    @Set void checked(bool value)
    {
        if (_checked == value)
            return;
        _checked = value;
        mixin(doChanged);
    }

    /// ditto
    @Get bool checked()
    {
        return _checked;
    }

    /**
     * Sets or gets the action caption.
     */
    @Set void caption(string value)
    {
        if (_caption == value)
            return;
        _caption = value;
        mixin(doChanged);
    }

    /// ditto
    @Get string caption()
    {
        return _caption[];
    }

    /**
     * Sets or gets the action hint.
     */
    @Set void hint(string value)
    {
        if (_hint == value)
            return;
        _hint = value;
        mixin(doChanged);
    }

    /// ditto
    @Get string hint()
    {
        return _hint[];
    }

    /**
     * Sets or gets the main shortcut.
     */
    @Set void shortcut(Shortcut value)
    {
        if (_shortcut1 == value)
            return;
        _shortcut1 = value;
        mixin(doChanged);
    }

    /// ditto
    @Get ref Shortcut shortcut()
    {
        return _shortcut1;
    }

    /**
     * Sets or gets the event called when the action is executed.
     *
     * This event can be used to avoid the declaration of any descendant
     * that would only override `procExecute`.
     */
    @Set void onExecute(Event value)
    {
        _onExecute = value;
    }

    /// ditto
    @Get Event onExecute()
    {
        return _onExecute;
    }

    /**
     * Sets or gets the event called when an action property has changed.
     *
     * This event is dedicated to the controls that take an Action as reference.
     */
    void onChanged(Event value)
    {
        _onChanged = value;
    }

    /// ditto
    Event onChanged()
    {
        return _onChanged;
    }
}

/**
 * Serializable collection of actions, with automatic life-time mangment.
 */
alias ActionList = PublishedObjectArray!Action;

unittest
{
    static assert(!MustAddGcRange!Action);
}
