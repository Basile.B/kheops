#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/**
 * This module contains layouts that implement specialized
 * realignment method, such as the AxisLayout or the FlowLayout.
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.layouts;

import
    std.algorithm;
import
    iz.properties, iz.memory, iz.enumset;
import
    kheops.types, kheops.control;


static this()
{
    registerFactoryClasses!(AxisLayout, SplitLayout)(classesRepository);
}

/**
 * Base class for the classes that skips default alignment of the children and
 * implements their own method to realign them.
 */
class Layout: Control
{

    mixin inheritedDtor;

public:

    override void realign()
    {
        _realignCount = 0;
        customRealignChildren;
    }
}

/**
 * A layout that align its children on an axis.
 */
final class AxisLayout: Layout
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Axis _axis;
    Margin _spacing = Margin(4,4,4,4);

protected:

    override void customRealignChildren()
    {
        const size_t cc = childrenCount;
        if (!cc) return;
        with (Axis) final switch (_axis)
        {
        case horizontal:
            const double w = sizeRect.width / cc;
            size_t i;
            foreach(CustomControl child; children)
            {
                child.beginRealign;
                child.alignment = Alignment.none;
                child.left = w * i + _spacing.left;
                child.width = w - _spacing.left - _spacing.right;
                child.top = _spacing.top;
                child.height = height - _spacing.top - _spacing.bottom;
                child.endRealign;
                ++i;
            }
            break;
        case vertical:
            const double h = sizeRect.height / cc;
            size_t i;
            foreach(CustomControl child; children)
            {
                child.beginRealign;
                child.alignment = Alignment.none;
                child.top = h * i + _spacing.top;
                child.height = h - _spacing.top - _spacing.bottom;
                child.left = _spacing.left;
                child.width = width - _spacing.left - _spacing.right;
                child.endRealign;
                ++i;
            }
            break;
        }
    }

public:

    ///
    this()
    {
        collectPublications!AxisLayout;
    }

    /**
     * Sets or gets the alignment axis.
     */
    @Set void axis(Axis value)
    {
        if (_axis == value)
            return;
        _axis = value;
        tryRealign;
    }

    /// ditto
    @Get Axis axis() {return _axis;}

    /**
     * Sets or gets the common left spacing.
     */
    @Set void spacingLeft(double value)
    {
        if (_spacing.left == value)
            return;
        _spacing.left = value;
        tryRealign;
    }

    /// ditto
    @Get double spacingLeft() {return _spacing.left;}

    /**
     * Sets or gets the common top spacing.
     */
    @Set void spacingTop(double value)
    {
        if (_spacing.top == value)
            return;
        _spacing.top = value;
        tryRealign;
    }

    /// ditto
    @Get double spacingTop() {return _spacing.top;}

    /**
     * Sets or gets the common right spacing.
     */
    @Set void spacingRight(double value)
    {
        if (_spacing.right == value)
            return;
        _spacing.right = value;
        tryRealign;
    }

    /// ditto
    @Get double spacingRight() {return _spacing.right;}

    /**
     * Sets or gets the common bottom spacing.
     */
    @Set void spacingBottom(double value)
    {
        if (_spacing.bottom == value)
            return;
        _spacing.bottom = value;
        tryRealign;
    }

    /// ditto
    @Get double spacingBottom() {return _spacing.bottom;}
}

/**
 * A layout that contains two splitted areas.
 */
class SplitLayout : Layout
{
    mixin PropertyPublisherImpl;
    mixin inheritedDtor;

private:

    double _splitPosition;
    double _splitterSize;
    double _downOffset;
    bool _splitDrag;
    Axis _splitAxis;

    Control _beforeControl;
    Control _afterControl;

    void clampSplitPosition()
    {
        _splitPosition = max(0, _splitPosition);
        if (_splitAxis == Axis.horizontal)
            _splitPosition = min(sizeRect.width, _splitPosition);
        else
            _splitPosition = min(sizeRect.height, _splitPosition);
    }

protected:

    override void customRealignChildren()
    {
        if (!_beforeControl || !_afterControl)
            return;

        _beforeControl.beginRealign;
        if (_splitAxis == Axis.horizontal)
        {
            const double wd = _splitPosition - _splitterSize * 0.5;
            _beforeControl.left = 0;
            _beforeControl.top = 0;
            _beforeControl.height = sizeRect.height;
            _beforeControl.width = wd;
        }
        else
        {
            const double hg = _splitPosition - _splitterSize * 0.5;
            _beforeControl.left = 0;
            _beforeControl.top = 0;
            _beforeControl.height = hg;
            _beforeControl.width = sizeRect.width;
        }
        _beforeControl.endRealign;

        _afterControl.beginRealign;
        if (_splitAxis == Axis.horizontal)
        {
            const double wd = sizeRect.width - _splitPosition + _splitterSize * 0.5;
            _afterControl.top = 0;
            _afterControl.left = _splitPosition + _splitterSize * 0.5;
            _afterControl.height = sizeRect.height;
            _afterControl.width = wd;
        }
        else
        {
            const double hg = sizeRect.height - _splitPosition + _splitterSize * 0.5;
            _afterControl.left = 0;
            _afterControl.top = _splitPosition + _splitterSize * 0.5;
            _afterControl.height = hg;
            _afterControl.width = sizeRect.width;
        }
        _afterControl.endRealign;
    }

    override void procMouseWheel(byte delta, const ref KeyModifiers km)
    {
        super.procMouseWheel(delta, km);

        beginRealign;
        splitPosition = _splitPosition + delta * 2;
        endRealign;
        repaint;
    }

    override void procMouseDown(double x, double y, const ref MouseButtons btn,
        const ref KeyModifiers km)
    {
        super.procMouseDown(x, y, btn, km);

        if (MouseButton.left !in btn)
            return;

        if (_splitAxis == Axis.horizontal)
        {
            if (x >= _splitPosition - _splitterSize * 0.5 &&
                x <= _splitPosition + _splitterSize * 0.5)
            {
                _splitDrag = true;
                _downOffset = x - _splitPosition;
            }
        }
        else
        {
            if (y >= _splitPosition - _splitterSize * 0.5 &&
                y <= _splitPosition + _splitterSize * 0.5)
            {
                _splitDrag = true;
                _downOffset = y - _splitPosition;
            }
        }
    }

    override void procMouseMove(double x, double y, const ref KeyModifiers km)
    {
        super.procMouseMove(x, y, km);
        if (_splitDrag)
        {
            if (_splitAxis == Axis.horizontal)
                splitPosition = x - _downOffset;
            else
                splitPosition = y - _downOffset;
        }
    }

    override void procMouseUp(double x, double y, const ref MouseButtons btn,
        const ref KeyModifiers km)
    {
        super.procMouseUp(x, y, btn, km);
        if (MouseButton.left !in btn)
            _splitDrag = false;
    }

public:

    this()
    {
        _splitterSize = 6;
        _splitPosition = 200;
        super();
        collectPublications!SplitLayout;
        _beforeControl = addControl!Control(Rect(0,0,1,1), Alignment.none);
        _afterControl  = addControl!Control(Rect(0,0,1,1), Alignment.none);
    }

    /**
     * Sets or gets which axis is splitted
     */
    @Set void splitAxis(Axis value)
    {
        if (_splitAxis == value)
            return;

        beginRealign;
        if (_splitAxis == Axis.horizontal)
        {
            const ratio =  (1.0f / width()) * _splitPosition;
            splitPosition = height() * ratio;
        }
        else
        {
            const ratio = (1.0f / height()) * _splitPosition;
            splitPosition = width() * ratio;
        }
        _splitAxis = value;
        endRealign;
        repaint;
    }

    /// ditto
    @Get Axis splitAxis() {return _splitAxis;}

    /**
     * Sets or gets the split position.
     */
    @Set void splitPosition(double value)
    {
        if (_splitPosition == value)
            return;
        _splitPosition = value;
        clampSplitPosition;
        tryRealign;
        repaint;
    }

    /// ditto
    @Get double splitPosition(){return _splitPosition;}

    /**
     * Sets or gets the size of the splitter.
     */
    @Set void splitterSize(double value)
    {
        if (_splitterSize == value)
            return;
        _splitterSize = value;
        tryRealign;
        repaint;
    }

    /// ditto
    @Get double splitterSize() {return _splitterSize;}

    /// Returns the layout located at the left or at the top.
    Control beforeControl(){return _beforeControl;}

    /// Returns the layout located at the right or at the bottom.
    Control afterControl(){return _afterControl;}
}

/**
 * A layout with a virtual content.
 */
class ScrollableLayout : Layout
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    bool _redirect;

protected:

    CustomControl _content;

public:

    ///
    this()
    {
        super();
        clipChildren = true;
        _content = addControl!Control(Rect(0,0,1,1), Alignment.none);
        _redirect = true;
        collectPublications!ScrollableLayout;
    }

    /// Operations on children are redirected to the content.
    override void addChild(CustomControl child)
    {
        if (!_redirect)
        {
            super.addChild(child);
        }
        else
        {
            _content.addChild(child);
        }
    }

    /// Operations on children are redirected to the content.
    override CustomControl removeChild(size_t index)
    {
        return _content.removeChild(index);
    }

    /// Operations on children are redirected to the content.
    override bool removeChild(CustomControl ctrl)
    {
        return _content.removeChild(ctrl);
    }

    /// Operations on children are redirected to the content.
    override void insertChild(CustomControl child)
    {
        _content.insertChild(child);
    }

    /// Operations on children are redirected to the content.
    override void insertChild(size_t index, CustomControl child)
    {
        _content.insertChild(index, child);
    }

    override void realign()
    {
        _realignCount = 0;
        if (_content)
            _content.realign();
    }

    /**
     * Sets or gets the content width.
     */
    @Set void contentWidth(double value)
    {
        _content.width = value;
    }

    /// ditto
    @Get double contentWidth(){return _content.width;}

    /**
     * Sets or gets the content height.
     */
    @Set void contentHeight(double value)
    {
        _content.height = value;
    }

    /// ditto
    @Get double contentHeight(){return _content.height;}

    /**
     * Sets or gets the content x coordinate.
     */
    @Set void scrollX(double value)
    {
        if (-value > 0)
            value = 0;
        if (-value + _content.width < width)
            value = _content.width - width;
        _content.left = -value;
    }

    /// ditto
    @Get double scrollX(){return -_content.left;}

    /**
     * Sets or gets the content y coordinate.
     */
    @Set void scrollY(double value)
    {
        if (-value > 0)
            value = 0;
        if (-value + _content.height < height)
            value = _content.height - height;
        _content.top = -value;
    }

    /// ditto
    @Get double scrollY(){return -_content.top;}

    CustomControl content(){return _content;}
}

unittest
{
    static assert(!MustAddGcRange!AxisLayout);
    static assert(!MustAddGcRange!SplitLayout);
    static assert(!MustAddGcRange!ScrollableLayout);
}
