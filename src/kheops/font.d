/**
 * Font
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.font;

import
    core.memory: GC;
import
    iz.types, iz.enumset, iz.properties, iz.memory;
import
    kheops.types, kheops.colors;

static this()
{
    classesRepository.registerFactoryClass!Font;
}

/**
 * Encapsulates the font properties.
 */
class Font: PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    version(linux)
        Array!char _name;
    version(Win32)
        static assert(0);
    double _size = 11;
    FontStyles _fontStyles;
    Event _onChanged;
    ptrdiff_t _changedCount;

    enum doChanged =
    q{
        if (_changedCount > 0)
            return;
        if (_onChanged)
            _onChanged(this);
        _changedCount = 0;
    };

public:

    ///
    this()
    {
        version(linux)
            name = "DejaVuSansMono.ttf";
        else
            static assert(0);
        collectPublications!Font;
    }

    ~this()
    {
        destruct(_name);
        callInheritedDtor;
    }

    /**
     * Copies the properties of another font.
     */
    void copyFrom(Font value)
    {
        beginChange;
        _size = value._size;
        _name = value._name;
        _fontStyles = value._fontStyles;
        endChange;
    }

    /**
     * Sets or gets the font size.
     */
    @Set void size(double value)
    {
        if(_size == value)
            return;
        _size = value;
        mixin(doChanged);
    }

    /// ditto
    @Get double size() {return _size;}

    /**
     * Sets or gets the font name.
     * The name should be unqualified and extension-less.
     * The full name is retieved in intern, using the paths
     * set in the font loader.
     */
    @Set void name(string value)
    {
        if (_name == value)
            return;
        import std.path;
        _name = value.baseName.stripExtension;
        mixin(doChanged);
    }

    /// ditto
    @Get string name() {return _name[];}

    /**
     * Sets or gets the font styles.
     */
    @Set void style(FontStyles value)
    {
        if (value == _fontStyles)
            return;
        _fontStyles = value;
        mixin(doChanged);
    }

    /// ditto
    @Get ref FontStyles style() {return _fontStyles;}

    /// Sets the styles using an array.
    void setStyles(const FontStyle[] value)
    {
        auto old = _fontStyles.container;
        _fontStyles = value;
        if (old != _fontStyles.container)
            mixin(doChanged);
    }

    /**
     * Signal fired when the font has changed.
     *
     * It usually indicates that a repaint is needed.
     */
    @Set void onChanged(Event value) {_onChanged = value;}

    /// ditto
    @Get Event onChanged(){return _onChanged;}

    /**
     * Manually triggers and forces the onChanged event.
     */
    void changed() {_changedCount = 0; mixin(doChanged);}

    /**
     * Prevents the onChanged event to be fired while several properties
     * are modified. Must be followed by a call to endChange.
     */
    void beginChange() {++_changedCount;}

    /// ditto
    void endChange()
    {
        --_changedCount;
        mixin(doChanged);
    }
}

unittest
{
    static assert(!MustAddGcRange!Font);
}

