#!runnable-flags: -L-lX11 -L-lcairo -L-lfreetype

/**
 * Canvas
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.canvas;

import
    std.math, std.stdio, std.string;
import
    iz.memory, iz.math;
import
    cairo.cairo, cairo.ft;
import
    kheops.types, kheops.control, kheops.fontloader;
public import
    kheops.colors, kheops.brush, kheops.pen, kheops.font, kheops.bitmap,
    kheops.pathdata;

alias PaintEvent = void delegate(Object notifier, const ref Rect rect, ref Canvas canvas);


/**
 *
 */
struct Canvas
{

private:

    enum BrushKind: ubyte {reset, fill, stroke}

    @NoGc cairo_t* _cr;

    ptrdiff_t _saveCount;

    Brush _fillBrush, _defFillBrush;
    Brush _strokeBrush, _defStrokeBrush;
    Font _font, _defFont;
    Pen _pen, _defPen;

    BrushKind _lastBrushedUsed;

    @NoGc cairo_pattern_t* _fillGradient;
    @NoGc cairo_pattern_t* _strokeGradient;
    cairo_matrix_t _gradScaler;
    cairo_font_extents_t _fe;

    void fillBrushChanged(Object notifier)
    {
        applyBrushToCairo!(BrushKind.fill);
        _lastBrushedUsed = BrushKind.reset;
    }

    void strokeBrushChanged(Object notifier)
    {
        applyBrushToCairo!(BrushKind.stroke);
        _lastBrushedUsed = BrushKind.reset;
    }

    void penChanged(Object notifier)
    {
        penToCairoData;
    }

    void fontChanged(Object notifier)
    {
        fontToCairoData;
    }

    // transforms Pen to cairo data, apply now
    void penToCairoData()
    {
        cairo_set_line_width(_cr, _pen.width);
        cairo_set_line_join(_cr, cast(CairoLineJoin) _pen.join);
        cairo_set_line_cap(_cr, cast(CairoLineCap) _pen.cap);
        final switch(_pen.dash)
        {
            case Dash.none:
                cairo_set_dash(_cr, null, 0, 0);
                break;
            case Dash.custom:
                cairo_set_dash(_cr, _pen.customDashing.ptr, cast(int) _pen.customDashing.length, 0);
                break;
            case Dash.dot1:
                cairo_set_dash(_cr, dashingDot1.ptr, cast(int) dashingDot1.length, 0);
                break;
            case Dash.dot2:
                cairo_set_dash(_cr, dashingDot2.ptr, cast(int) dashingDot2.length, 0);
                break;
            case Dash.dot4:
                cairo_set_dash(_cr, dashingDot4.ptr, cast(int) dashingDot4.length, 0);
                break;
            case Dash.dash2:
                cairo_set_dash(_cr, dashingDash2.ptr, cast(int) dashingDash2.length, 0);
                break;
            case Dash.dash4:
                cairo_set_dash(_cr, dashingDash4.ptr, cast(int) dashingDash4.length, 0);
                break;
            case Dash.dashdot1:
                cairo_set_dash(_cr, dashingDashDot1.ptr, cast(int) dashingDashDot1.length, 0);
                break;
            case Dash.dashdot2:
                cairo_set_dash(_cr, dashingDashDot2.ptr, cast(int) dashingDashDot2.length, 0);
                break;
            case Dash.dashdot4:
                cairo_set_dash(_cr, dashingDashDot4.ptr, cast(int) dashingDashDot4.length, 0);
                break;
        }
    }

    // apply brush transformed to cairo data
    void applyBrushToCairo(BrushKind k)()
    {
        static if (k == BrushKind.fill)
        {
            alias br = _fillBrush;
            alias pt = _fillGradient;
        }
        else static if (k == BrushKind.stroke)
        {
            alias br = _strokeBrush;
            alias pt = _strokeGradient;
        }
        else static assert(0, "BrushKind.empty does not indicate a brush");

        _lastBrushedUsed = k;

        with (br) if (fillKind == FillKind.gradient)
        {
            if (pt)
            {
                cairo_pattern_destroy(pt);
                pt = null;
            }
            if (gradientKind == GradientKind.horizontal)
                pt = cairo_pattern_create_linear(0, 0, 1, 0);
            else if (gradientKind == GradientKind.vertical)
                pt = cairo_pattern_create_linear(0, 0, 0, 1);
            else
                pt = cairo_pattern_create_radial(0.5, 0.5, 0.0, 0.5, 0.5, 0.5) ;

            assert(cairo_pattern_status(pt) == CairoStatus.Success);

            foreach(immutable i; 0 .. gradient.stopCount)
            {
                cairo_pattern_add_color_stop_rgba(pt,
                    gradient.positions[i], Rgba(gradient.colors[i]).rgbaList[0..$]);
            }
        }
        with(FillKind) final switch (br.fillKind)
        {
            case none:
                cairo_set_source_rgba(_cr, .0, .0, .0, .0);
                break;
            case uniform:
                cairo_set_source_rgba(_cr, br.rgba.rgbaList[0..$]);
                break;
            case gradient:
                cairo_pattern_set_matrix(pt, &_gradScaler);
                cairo_set_source(_cr, pt);
                cairo_pattern_set_extend(pt, cast(CairoExtend) br.fillAdjustment);
                assert(cairo_pattern_status(pt) == CairoStatus.Success);
                break;
            case bitmap:
                cairo_set_source_surface(_cr, br.bitmap.surface, 0 , 0);
                auto p = cairo_get_source(_cr);
                cairo_pattern_set_extend(p, cast(CairoExtend) br.fillAdjustment);
                break;
        }
    }

    // transforms Font to cairo data, apply now.
    void fontToCairoData()
    {
        cairo_font_face_t* ff;
        ff = getCairoFont(_font.name);
        if (!ff)
        {
            CairoFontSlant cfs = (FontStyle.italic in _font.style) ?
                CairoFontSlant.Italic : CairoFontSlant.Normal;
            CairoFontWeight cfw = (FontStyle.bold in _font.style) ?
                CairoFontWeight.Bold : CairoFontWeight.Normal;
            ff = cairo_toy_font_face_create("cairo:monospace", cfs, cfw);
        }
        cairo_set_font_face(_cr, ff);
        cairo_set_font_size(_cr, _font.size);
        cairo_ft_font_face_set_synthesize(ff, _font.style.container & 3);
        cairo_font_extents(_cr, &_fe);

        assert(cairo_font_face_status(ff) == CairoStatus.Success,
            cairo_font_face_status(ff).cairo_status_to_string.fromStringz);
    }

package:

    /**
     * Called by an OsWindow to initialize the canvas when the context changes.
     */
    void setContext(cairo_t* context)
    {
        _cr = context;
        _lastBrushedUsed = BrushKind.reset;

        if (!_defFillBrush)
        {
            _defFillBrush = construct!Brush;
            _defFillBrush.color = defaultFillColor;
            _defFillBrush.onChanged = &fillBrushChanged;

            _defStrokeBrush = construct!Brush;
            _defStrokeBrush.color = defaultStrokeColor;
            _defStrokeBrush.onChanged = &strokeBrushChanged;

            _defFont = construct!Font;
            _defFont.onChanged = &fontChanged;

            _defPen = construct!Pen;
            _defPen.onChanged = &penChanged;
        }

        pen = _defPen;
        strokeBrush = _defStrokeBrush;
        fillBrush = _defFillBrush;
        font = _defFont;
    }

public:

    @disable this(this);

    ~this()
    {
        //cairo-pattern.c:1127: cairo_pattern_destroy: Assertion `CAIRO_REFERENCE_COUNT_HAS_REFERENCE (&pattern->ref_count)' failed.
        if (_fillGradient && _fillGradient.cairo_pattern_get_reference_count)
            cairo_pattern_destroy(_fillGradient);
        if (_strokeGradient && _strokeGradient.cairo_pattern_get_reference_count)
            cairo_pattern_destroy(_strokeGradient);

        destructEach(_defFillBrush, _defStrokeBrush, _defFont, _defPen);
    }

    // props ---+

    /// Restores the default brushes, font and pen.
    void restoreAllDefaults()
    {
        restoreDefaultFillBrush;
        restoreDefaultFont;
        restoreDefaultPen;
        restoreDefaultStrokeBrush;
    }

    /**
     * Sets or Gets the reference to the brush used to fill a path.
     *
     * By default this value points to an internal Brush. After replacing
     * the reference, it can be restored with restoreDefaultFillBrush().
     * It's also possible to replace all the values of the internal brush
     * using copyFrom().
     *
     * The default brush onChanged() event is part of the internal signaling
     * system and must never be modifed.
     */
    void fillBrush(Brush value)
    {
        if (!value)
            fillBrush(_defFillBrush);
        if (_fillBrush == value)
            return;
        _fillBrush = value;
        applyBrushToCairo!(BrushKind.fill);
    }

    /// ditto
    Brush fillBrush() {return _fillBrush;}

    /// ditto
    void restoreDefaultFillBrush() {fillBrush = _defFillBrush;}

    /**
     * Sets or Gets the reference to the brush used to stroke a path.
     *
     * By default this value points to an internal Brush. After replacing
     * the reference, it can be restored with restoreDefaultStrokeBrush().
     * It's also possible to replace all the values of the internal brush
     * using copyFrom().
     *
     * The default brush onChanged() event is part of the internal signaling
     * system and must never be modifed.
     */
    void strokeBrush(Brush value)
    {
        if (!value)
            strokeBrush(_defStrokeBrush);
        if (_strokeBrush == value)
            return;
        _strokeBrush = value;
        applyBrushToCairo!(BrushKind.stroke);
    }

    /// ditto
    Brush strokeBrush() {return _strokeBrush;}

    /// ditto
    void restoreDefaultStrokeBrush() {strokeBrush = _defStrokeBrush;}

    /**
     * Sets or Gets the reference to the font used to draw text.
     *
     * By default this value points to an internal Font. After replacing
     * the reference, it can be restored with restoreDefaultFont().
     * It's also possible to replace all the values of the internal font
     * using copyFrom().
     *
     * The default font onChanged() event is part of the internal signaling
     * system and must never be modifed.
     */
    void font(Font value)
    {
        if (!value)
            font(_defFont);
        if (_font == value)
            return;
        _font = value;
        fontToCairoData;
    }

    /// ditto
    Font font() {return _font;}

    /// ditto
    void restoreDefaultFont() {font = _defFont;}

    /**
     * Sets or Gets the reference to the pen used to stroke a path.
     *
     * By default this value points to an internal Pen. After replacing
     * the reference, it can be restored with restoreDefaultPen().
     * It's also possible to replace all the values of the internal pen
     * using copyFrom().
     *
     * The default pen onChanged() event is part of the internal signaling
     * system and must never be modifed.
     */
    void pen(Pen value)
    {
        if (!value)
            pen(_defPen);
        if (_pen == value)
            return;
        _pen = value;
        penToCairoData;
    }

    /// ditto
    Pen pen() {return _pen;}

    /// ditto
    void restoreDefaultPen() {pen = _defPen;}
    // ----

    // translation ---+

    /**
     * Saves the state of the canvas
     *
     * Note that the state of the transformations is usually handled
     * automatically by beginControl() and endControl().
     *
     * Returns:
     *      A value that indicates how many times the canvas can be restored.
     */
    ptrdiff_t save()
    {
        cairo_save(_cr);
        return _saveCount++;
    }

    /**
     * Restores the canvas to a previous state.
     *
     * Note that the state of the transformations is usually handled
     * automatically by beginControl() and endControl().
     *
     * Params:
     *      count = Specifies how many times the canvas will be restored.
     *      By default it's always restored once.
     */
    void restore(ptrdiff_t count = 1)
    {
        foreach(immutable i; 0 .. count)
        {
            if (_saveCount == 0)
                break;
            cairo_restore(_cr);
            _saveCount--;
        }
    }

    /**
     * Translates the canvas, scale the gradients or the textures,
     * according to the position of a control.
     *
     * This method is automatically called by an orphan control,
     * usually an OsWindow.
     */
    void beginControl(CustomControl ctrl)
    {
        auto cc = ctrl.parentedRectList;
        cairo_matrix_init_scale(&_gradScaler, 1/cc[2], 1/cc[3]);
        cairo_transform(_cr, ctrl.matrix!false);
        if (ctrl.clipChildren)
        {
            cairo_rectangle(_cr, -1, -1, cc[2]+2, cc[3]+2);
            cairo_clip(_cr);
        }
    }

    void scaleGradient(CustomControl ctrl)
    {
        auto sz = ctrl.sizeList;
        cairo_matrix_init_scale(&_gradScaler, 1/sz[0], 1/sz[1]);
    }

    /**
     * Restores the canvas geometry when a control has finished to paint itself.
     *
     * This method is automatically called by an orphan control,
     * usually an OsWindow.
     */
    void endControl(CustomControl ctrl)
    {
        cairo_transform(_cr, ctrl.matrix!true);
        if (ctrl.clipChildren)
            cairo_reset_clip(_cr);
    }

    // ----

    // path building ---+

    /// Begins a new path at x, y.
    void beginPath(double x, double y)
    {
        cairo_move_to(_cr, x,y);
        cairo_new_sub_path(_cr);
    }

    /// Closes the current path.
    void closePath()
    {
        cairo_close_path(_cr);
    }

    /// Moves to x, y.
    void move(double x, double y)
    {
        cairo_move_to(_cr, x, y);
    }

    /// Adds an arc to the path.
    void arc(double cx, double cy, double radius, double angle1, double angle2)
    {
        cairo_arc(_cr, cx, cy, radius, angle1, angle2);
    }

    /// Adds a line to the path, starting from current position to x,y.
    void line(double x, double y)
    {
        cairo_line_to(_cr, x, y);
    }

    /// Adds a cubic Bézier curve from the current point to the endPoint using two control points
    void curve(T : Point)(auto ref const(T) ctrl1, auto ref const(T) ctrl2,
        auto ref const(T) point)
    {
        cairo_curve_to(_cr, ctrl1.x, ctrl1.y, ctrl2.x, ctrl2.y, point.x, point.y);
    }
    // ----

    // full, closed shapes ---+
    /**
     * Appends a circle.
     */
    void circle(double cx, double cy, double radius)
    {
        cairo_new_path(_cr);
        cairo_arc(_cr, cx, cy, radius, 0.0f, Pi!2);
        cairo_close_path(_cr);
    }

    /**
     * Appends an ellipse.
     */
    void ellipse(double cx, double cy, double radius, double wr, double hr)
    {
        cairo_new_path(_cr);
        cairo_save(_cr);
        cairo_scale(_cr, wr, hr);
        cairo_arc(_cr, cx / wr, cy / hr, radius, 0.0f, Pi!2);
        cairo_restore(_cr);
        cairo_close_path(_cr);
    }

    /**
     * Appends a pie.
     */
    void pie(double cx, double cy, double radius, double angleBeg, double angleEnd)
    {
        cairo_new_path(_cr);
        cairo_arc(_cr, cx, cy, radius, angleBeg, angleEnd);
        cairo_line_to(_cr, cx, cy);
        cairo_close_path(_cr);
    }

    /**
     * Appends a rectangle.
     */
    void rectangle(double x, double y, double w, double h)
    {
        cairo_new_path(_cr);
        cairo_rectangle(_cr, x, y, w, h);
        cairo_close_path(_cr);
    }

    /// ditto
    void rectangle(R)(const auto ref R rect)
    {
        rectangle(rect.tupleof[0..$]);
    }

    /**
     * Appends an enhanced rectangle.
     *
     * Params:
     *      rect = The rectangle area.
     *      radius = The corners radius.
     *      topLeft = The top left corner kind.
     *      topRight = The top right corner kind.
     *      BottomRight = The bottom right corner kind.
     *      BottomLeft = The bottom left corner kind.
     */
    void rectangleEn(C : CornerKind)(const auto ref Rect rect, double radius,
        C topLeft, C topRight, C BottomRight, C BottomLeft)
    {
        with (CornerKind)
        {
            final switch(topRight)
            {
                case none:
                    beginPath(rect.left, rect.top);
                    cairo_line_to(_cr, rect.width, rect.top);
                    break;
                case line:
                    beginPath(rect.left + radius, rect.top);
                    cairo_line_to(_cr, rect.right() - radius, rect.top);
                    cairo_line_to(_cr, rect.right(), rect.top + radius);
                    break;
                case round:
                    cairo_arc(_cr, rect.left + rect.width - radius,
                        rect.top + radius,
                        radius, -Pi!(1,2), 0.0f);
            }
            final switch(BottomRight)
            {
                case none:
                    cairo_line_to(_cr, rect.width, rect.height);
                    break;
                case line:
                    cairo_line_to(_cr, rect.right(), rect.bottom() - radius);
                    cairo_line_to(_cr, rect.right() - radius, rect.bottom());
                    break;
                case round:
                    cairo_arc(_cr, rect.left + rect.width - radius,
                        rect.top + rect.height - radius,
                        radius, 0.0f, Pi!(1,2));
            }
            final switch(BottomLeft)
            {
                case none:
                    cairo_line_to(_cr, rect.left, rect.height);
                    break;
                case line:
                    cairo_line_to(_cr, rect.left + radius, rect.bottom());
                    cairo_line_to(_cr, rect.left, rect.bottom() - radius);
                    break;
                case round:
                    cairo_arc(_cr, rect.left + radius,
                        rect.bottom() - radius,
                        radius, Pi!(1,2), Pi!1);
            }
            final switch(topLeft)
            {
                case none:
                    cairo_line_to(_cr, rect.left, rect.top);
                    break;
                case line:
                    cairo_line_to(_cr, rect.left, radius + rect.top);
                    cairo_line_to(_cr, rect.left + radius, rect.top);
                    break;
                case round:
                    cairo_arc(_cr, rect.left + radius, rect.top + radius,
                        radius, Pi!1, Pi!(3,2));
            }
        }
        closePath;
    }

    /**
     * Appends a polygon.
     */
    void polygon(P)(const auto ref P points)
    {
        if (!points.length)
            return;

        cairo_new_path(_cr);
        cairo_move_to(_cr, points[0].x, points[0].y);
        foreach(immutable i; 1 .. points.length)
            cairo_line_to(_cr, points[i].x, points[i].y);
        cairo_close_path(_cr);
    }

    /**
     * Appends a path described by a PathData.
     */
    void drawPath(P)(auto ref P path)
    {
        cairo_move_to(_cr, 0, 0);
        cairo_new_sub_path(_cr);
        foreach(ref const(PathPoint!double) pt; path.range())
        {
            final switch(pt.kind)
            {
            case PathPointKind.move:
                cairo_move_to(_cr, pt.points[0].x, pt.points[0].y);
                break;
            case PathPointKind.line:
                cairo_line_to(_cr, pt.points[0].x, pt.points[0].y);
                break;
            case PathPointKind.curve:
                cairo_curve_to(_cr, pt.points[0].x, pt.points[0].y, pt.points[1].x,
                                pt.points[1].y, pt.points[2].x, pt.points[2].y);
                break;
            case PathPointKind.close:
                cairo_close_path(_cr);
                break;
            // already turned into regular lines
            case PathPointKind.horzLine, PathPointKind.vertLine:
                break;
            }
        }
    }

    /**
     * Appends the path for a text.
     * Contrary to text(), it can be filled.
     */
    void drawText(double x, double y, string text)
    {
        cairo_move_to(_cr, x, y);
        cairo_text_path(_cr, text.toStringz);
    }

    /**
     * Draws an image with optionaly stretched in the input rectangle.
     */
    void drawImage(const ref Rect rect, Bitmap bmp, Stretch stretch = Stretch.none)
    {
        cairo_save(_cr);
        scope(exit) cairo_restore(_cr);

        cairo_rectangle(_cr, rect.left, rect.top, rect.width, rect.height);
        final switch(stretch)
        {
            case Stretch.none: break;
            case Stretch.scaled:
                const double sc = (rect.width > rect.height) ? rect.height / bmp.height:
                    rect.width / bmp.width;
                cairo_scale(_cr, sc, sc);
                break;
            case Stretch.unscaled:
                cairo_scale(_cr, rect.width / bmp.width, rect.height / bmp.height);
        }

        cairo_set_source_surface(_cr, bmp.surface, 0, 0);
        cairo_fill(_cr);
    }
    // ----

    // text ---+
    /**
     * Appends a simple text using the pen and the fillBrush.
     */
    void text(double x, double y, const(char)[] text)
    {
        cairo_move_to(_cr, x, y);
        cairo_show_text(_cr, text.toStringz);
    }

    /**
     * Appends a simple text using the pen and the fillBrush.
     *
     * In this overload rect represents the rectangle in which
     * the justification is applied.
     */
    void text(Rect rect, const(char)[] text, Justify just = Justify.left)
    {
        immutable(char)* textz = text.toStringz;
        cairo_text_extents_t te = void;
        cairo_text_extents(_cr, textz, &te);

        double x = void;
        const double y = rect.height - (rect.height - te.height) * 0.5;

        final switch(just)
        {
            case Justify.left:
                x = rect.left;
                break;
            case Justify.center:
                x = (rect.width - rect.left - te.width) * 0.5;
                break;
            case Justify.right:
                x = rect.left + rect.width - te.width;
                break;
        }

        cairo_move_to(_cr, x, y);
        cairo_show_text(_cr, textz);
    }

    /**
     * Returns: a TextExtent that represents the width and the height of a string,
     * as if drawn on the canvas.
     */
    TextExtent textSize(const(char)[] text)
    {
        cairo_text_extents_t te = void;
        cairo_text_extents(_cr, text.toStringz, &te);
        return TextExtent(te.x_advance, te.height, te.x_bearing, te.y_bearing);
    }

    /**
     * Returns: The Y coordinate of the base line where to draw some text.
     */
    double textBaseLineY(double height){return (height +_fe.height) * 0.5 - _fe.descent;}

    /**
     * Returns: The height of the tallest character.
     */
    double maxTextHeight(){return _fe.ascent + _fe.descent;}

    /**
     * Returns: The width of the widest character.
     */
    double maxCharWidth(){return _fe.max_x_advance;}
    // ----

    // caching ---+
    /**
     * Appends a path using the data previously saved with savePath().
     */
    void restorePath(cairo_path_t* value)
    {
        cairo_append_path(_cr, value);
    }

    /**
     * Saves the current path.
     */
    cairo_path_t* savePath()
    {
        return cairo_copy_path(_cr);
    }

    // ----

    // painting ---+
    /**
     * Strokes the current path using strokeBrush and pen parameters.
     *
     * Params:
     *      finish = False by default, if set to true then the path is removed
     *      from the context. The last time stroke or fill is called the
     *      parameter must be set to true.
     */
    void stroke(bool finish = false)()
    {
        applyBrushToCairo!(BrushKind.stroke);
        penToCairoData;
        static if (finish)
            cairo_stroke(_cr);
        else
            cairo_stroke_preserve(_cr);
    }

    /**
     * Fills the current path using fillBrush parameters.
     *
     * Params:
     *      finish = False by default, if set to true then the path is removed
     *      from the context. The last time stroke or fill is called the
     *      parameter must be set to true.
     */
    void fill(bool finish = false)()
    {
        applyBrushToCairo!(BrushKind.fill);
        static if (finish)
            cairo_fill(_cr);
        else
            cairo_fill_preserve(_cr);
    }

    // ----
}

unittest
{
    static assert(!MustAddGcRange!Canvas);
}

