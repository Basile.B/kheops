/**
 * Styles
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt).
 */
module kheops.styles;

import
    iz.properties, iz.serializer, iz.streams, iz.classes, iz.memory,
    iz.containers;
import
    kheops.types, kheops.control;

/// The current styles.
///__gshared Styles stylesStock;

/++

/**
 * The Styles class is a serializable Style collection.
 */
final class Styles
{

private:

    PublishedObjectArray!Style _styles;

    void loaderWantAgg(IstNode node, ref void* agg, out bool fromRefererence)
    {
    }

    void loaderWantDescr(IstNode node, ref Ptr descriptor, out bool stop)
    {
    }

public:

    ///
    this()
    {
        _styles = construct!(PublishedObjectArray!Style);
    }

    ~this()
    {
        destruct(_styles);
    }

    /**
     * Adds a new style.
     */
    CustomControl addStyle()
    {
        Style result = _styles.addItem;
        result._styles = this;
        return result;
    }

    /**
     * Saves the collection to a file.
     */
    void saveToFile(const char[] filename)
    {
        publisherToFile(_styles, filename);
    }

    /**
     * Loads the collection from a file.
     */
    void loadFromFile(const char[] filename)
    {
        fileToPublisher(filename, _styles, SerializationFormat.iztxt,
            &loaderWantAgg, &loaderWantDescr);
    }

    /**
     * Saves the collection to a Stream.
     */
    void saveToStream(Stream str)
    {
        Serializer ser = construct!Serializer;
        scope(exit) destruct(ser);
        ser.publisherToStream(_styles, str);
    }

    /**
     * Loads the collection from a Stream.
     */
    void loadFromStream(Stream str)
    {
        Serializer ser = construct!Serializer;
        scope(exit) destruct(ser);
        ser.onWantDescriptor = &loaderWantDescr;
        ser.onWantAggregate =  &loaderWantAgg;
        ser.streamToPublisher(str, _styles);
    }

    /**
     * Removes and deletes a style from the collection.
     */
    void deleteStyle(size_t index)
    {
        _styles.deleteItem!size_t(index);
    }

    /// ditto
    void deleteStyle(Style stl)
    {
        _styles.deleteItem!Style(stl);
    }

    /**
     * Returns the count of Style within the collection.
     */
    size_t count() {return _styles.count;}

    /**
     * Returns the Style matching to a particular ID.
     */
    CustomControl findStyle(string id)
    {
        foreach(Style stl; _styles)
            if (stl.styleID == id)
                return stl;
        return null;
    }
}

/**
 * A Style contains a compound of primitives that's used to define
 * the visual aspect of a particular class of CustomControl.
 *
 * The link between a CustomControl class and its Style is established through
 * an unique identifier.
 */
class Style: Control
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Array!char _styleID;
    Styles _styles;

public:

    ///
    this()
    {
        collectPublications!Style;
    }

    ~this()
    {
        destructOwnedPublishers(this);
    }

    void updatePublications()
    {
        collectPublications!Style;
    }

    /**
     * Sets or gets the unique ID for this style.
     */
    @Set void styleID(string value)
    in
    {
        assert(_styles);
    }
    body
    {
        if (_styles.findStyle(value))
            return;
        _styleID = value;
    }

    /// ditto
    @Get string styleID() {return _styleID[];}

    /**
     * A Style cannot be stylized.
     * The setter is a noop and the getter always returns an empty string.
     */
    override void stylerID(string value){}

    /// ditto
    override string stylerID() {return "";}
}
++/
