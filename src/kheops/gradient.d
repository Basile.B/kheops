/**
 * Gradient
 *
 * Authors: Basile B.
 *
 * License:
 *  Boost Software License, Version 1.0 (http://www.boost.org/LICENSE_1_0.txt)
 */
module kheops.gradient;

import
    iz.sugar, iz.properties, iz.containers, iz.memory;
import
    kheops.types, kheops.colors;

/**
 * Data of a gradient.
 */
class Gradient : PropertyPublisher
{
    mixin inheritedDtor;
    mixin PropertyPublisherImpl;

private:

    Array!double _positions;
    Array!uint _colors;
    Event _onChanged;
    ptrdiff_t _changedCount;

    enum doChanged =
    q{
        if (_changedCount > 0)
            return;
        if (_onChanged)
            _onChanged(null);
        _changedCount = 0;
    };

    invariant {assert(_colors.length == _positions.length);}

public:

    ///
    this()
    {
        collectPublications!Gradient;
    }

    ~this()
    {
        destructEach(_positions, _colors);
        callInheritedDtor;
    }

    /**
     * Resets the gradient positions and colors.
     */
    void clear()
    {
        _positions.reset;
        _colors.reset;
    }

    /**
     * Copies the data from another Gradient.
     */
    void copyFrom(Gradient value)
    {
        _positions = value._positions;
        _colors = value._colors;
        mixin(doChanged);
    }

    /**
     * Sets or gets the gradient positions
     */
    @Set void positions(double[] value)
    {
        if (value == _positions)
            return;
        _positions = value;
       if (_colors.length != _positions.length)
            _colors.length = _positions.length;
        mixin(doChanged);
    }
    /// ditto
    @Get double[] positions() {return _positions[];}

    /**
     * Sets or gets the gradient colors.
     */
    @Set void colors(uint[] value)
    {
        if (value == _colors)
            return;
        _colors = value;
        if (_positions.length != _colors.length)
            _positions.length = _colors.length;
        mixin(doChanged);
    }
    /// ditto
    @Get uint[] colors() {return _colors[];}

    /**
     * Inserts a stop.
     *
     * Params:
     *      color = A color.
     *      position = A position, must be a value between 0.0 and 1.0.
     *      index = The index of the gradient. An invalid value is either
     *      converted to 0 (first position) or stopCount-1 (last position).
     */
    void insertStop(ptrdiff_t index, double position, uint color)
    {
        import std.array: insertInPlace;
        if (index < 0) index = 0;
        else if (index > _colors.length) index = _colors.length;
        const uint[] cl = _colors[0..index];
        const uint[] cr = _colors[index..$];
        _colors = cl ~ color ~ cr;
        const double[] pl = _positions[0..index];
        const double[] pr = _positions[index..$];
        _positions = pl ~ position ~ pr;
        mixin(doChanged);
    }

    /**
     * Deletes a stop.
     *
     * Params:
     *      index = The index of the stop to delete. When an invalid index is
     *      passed, nothing is done.
     */
    void deleteStop(ptrdiff_t index)
    {
        import std.algorithm.mutation: remove;
        if (index < 0 || index >= _colors.length) return;
        _colors = _colors[0..index] ~ _colors[index+1..$];
        _positions = _positions[0..index] ~ _positions[index+1..$];
        mixin(doChanged);
    }

    /**
     * Returns the count of stop.
     */
    size_t stopCount() {return _colors.length;}

    /**
     * Signal fired when the gradient has changed.
     *
     * It usually indicates that a repaint is needed.
     */
    void onChanged(Event value) {_onChanged = value;}

    /// ditto
    Event onChanged(){return _onChanged;}

    /**
     * Manually triggers and forces the onChanged event.
     */
    void changed() {_changedCount = 0; mixin(doChanged);}

    /**
     * Prevents the onChanged event to be fired while several properties
     * are modified. Must be followed by a call to endChange.
     */
    void beginChange() {++_changedCount;}

    /// ditto
    void endChange()
    {
        --_changedCount;
        mixin(doChanged);
    }
}

unittest
{
    Gradient g0;
    g0.insertStop(Rgba(0).color, 0.1, 0);
    g0.insertStop(Rgba(0).color, 0.9, 2);
    g0.deleteStop(1);
    assert(g0._colors.length == 1);
}

unittest
{
    import iz.memory: MustAddGcRange;
    static assert(!MustAddGcRange!Gradient);
}

